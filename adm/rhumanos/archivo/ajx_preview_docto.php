<?php
/**
 * Complemento del llamado ajax para eliminar los archivos en una carpeta temporal del servidor.
 * Lista de parámetros recibidos por POST 
 * @param String file, contiene el nombre del archivo a eliminar en el servidor.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    $path_exped = 'adm/expediente/doctos/';	        	
    
    $datos = explode("-", $_POST["prm"]);
    $img = $path_exped . $_SESSION["xCurp"] . '/' . $_SESSION["xCurp"] . '_' . $datos[0] . '_' . $datos[1] . '_' . $datos[2] . $datos[3]; 
    $html = '<img src="' . $img . '" alt="img_doc" style="max-width: 950px;" />';
    
    echo $html;
}
?>