<?php
/**
 * Complemento del llamado ajax para dar de baja a un aspirante 
 * @param string id(curp), recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_aspirantes.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objAspi = new AdmtblAspirantes();
    
    if( $objAspi->delete($_GET["id"]) ){
        $ajx_datos["rslt"] = true;
        $ajx_datos["error"] = "xx";
        $objSys->registroLog($objUsr->idUsr, "admtbl_aspirantes", $_GET["id"], "Dlt");
    } else {
        $ajx_datos["rslt"] = false;
        $ajx_datos["error"] = utf8_encode($objAspi->msjError);
    }
    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>