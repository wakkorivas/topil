<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_datos_personales.class.php';
include 'includes/class/admtbl_doctos_ident.class.php';
$objDocIdent = new AdmtblDoctosIdent();
$objDocIdent->select($_SESSION["xCurp"]);

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/file_uploader/AjaxUpload.2.0.min.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/doc_ident.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('persona_menu');
?>
    <style type="text/css">
        .spnTituloDoc{ background-color: #d7d7d7; border: 1px solid gray; border-radius: 4px; display: block; font-size: 10pt; font-weight: bold; padding: 3px; width: auto; }
        .dvFoto{ border: 2px solid gray; cursor: pointer; height: 225px; margin: auto auto; width: 170px; }
        .dvFoto img{ height: 225px; width: 170px; }
        .dvFirma{ border: 2px solid gray; cursor: pointer; height: 140px; margin: auto auto; width: 300px; }
        .dvFirma img{ height: 140px; width: 300px; }
        .dvHuella{ border: 2px solid gray; height: 165px; width: 150px; }
        .dvHuella img{ height: 165px; width: 150px; }
    </style>
    
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php
                    if( $objDocIdent->AdmtblDatosPersonales->id_status != 2 ){// diferente de BAJA
                    ?>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los cambios realizados...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <?php
                    }
                    ?>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la modificación de datos...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php        
    $path_docs = 'adm/expediente/';
    $foto_fte   = ( !empty($objDocIdent->foto_frente) )   ? '<img src="' . $path_docs . 'fotos/' . $objDocIdent->foto_frente . '" alt="Foto de frente">'  : '';
    $foto_izq   = ( !empty($objDocIdent->foto_izq) )   ? '<img src="' . $path_docs . 'fotos/' . $objDocIdent->foto_izq . '" alt="Foto perfil izquierdo">'  : '';
    $foto_der   = ( !empty($objDocIdent->foto_der) )   ? '<img src="' . $path_docs . 'fotos/' . $objDocIdent->foto_der . '" alt="Foto perfil derecho">'  : '';
    $firma      = ( !empty($objDocIdent->firma) )   ? '<img src="' . $path_docs . 'firmas/' . $objDocIdent->firma . '" alt="Firma">'  : '';
    $huella_izq = ( !empty($objDocIdent->huella_pulg_izq) )   ? '<img src="' . $path_docs . 'huellas/' . $objDocIdent->huella_pulg_izq . '" alt="Pulgar izquierdo">'  : '';
    $huella_der = ( !empty($objDocIdent->huella_pulg_der) )   ? '<img src="' . $path_docs . 'huellas/' . $objDocIdent->huella_pulg_der . '" alt="Pulgar derecho">'  : '';
    ?>
    <form id="frmRegistro" method="post" action="#" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 750px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Control de Documentos de Indentidad</span>
            <fieldset class="fsetForm-Data" style="width: 700px;">                                
                <table id="tbForm-DatPer" class="tbForm-Data" style="width: 700px;"> 
                    <tr>
                        <td colspan="3" style=""><span class="spnTituloDoc">FOTOGRAFÍAS</span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <span>PERFIL IZQUIERDO</span>
                            <div id="dvFotoIzq" class="dvFoto">
                                <?php echo $foto_izq;?>
                            </div>
                            <input type="hidden" name="hdnFotoIzq" id="hdnFotoIzq" value="" />
                        </td>
                        <td style="text-align: center;">
                            <span>FRENTE</span>
                            <div id="dvFotoFte" class="dvFoto">
                                <?php echo $foto_fte;?>
                            </div>
                            <input type="hidden" name="hdnFotoFte" id="hdnFotoFte" value="" />
                        </td>
                        <td style="text-align: center;">
                            <span>PERFIL DERECHO</span>
                            <div id="dvFotoDer" class="dvFoto">
                                <?php echo $foto_der;?>
                            </div>
                            <input type="hidden" name="hdnFotoDer" id="hdnFotoDer" value="" />
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="3" style=""><span class="spnTituloDoc">FIRMA</span></td>
                    </tr>             
                    <tr>
                        <td colspan="3" style="text-align: center;">
                            <div id="dvFirma" class="dvFirma">
                                <?php echo $firma;?>
                            </div>
                            <input type="hidden" name="hdnFirma" id="hdnFirma" value="" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style=""><span class="spnTituloDoc">HUELLAS DACTILARES</span></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center;">
                            <span>PULGAR IZQUIERDO</span>
                            <div id="dvHuellaIzq" class="dvHuella" style="margin: auto auto;">
                                <?php echo $huella_izq;?>
                            </div>
                            <input type="hidden" name="hdnHuellaIzq" id="hdnHuellaIzq" value="" />
                        </td>
                        <td style="text-align: left;">
                            <span>&nbsp;&nbsp;PULGAR DERECHO</span>
                            <div id="dvHuellaDer" class="dvHuella">
                                <?php echo $huella_der;?>
                            </div>
                            <input type="hidden" name="hdnHuellaDer" id="hdnHuellaDer" value="" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <input type="hidden" id="hdnUrlUpload" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_upload.php');?>" />
        <input type="hidden" id="hdnUrlDirTemp" value="<?php echo $objSys->encrypt('adm/_uploadfiles/');?>" />
        <input type="hidden" id="hdnUrlSave" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_reg_doc_ident.php');?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>