<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admcat_nivel_estudios.class.php';
include 'includes/class/admtbl_nivel_estudios.class.php';

$objCatNivelEst = new AdmcatNivelEstudios();
$objNivelEst = new AdmtblNivelEstudios();
$objNivelEst->select($_SESSION['xCurp']);

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/nivelest.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('persona_menu');
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('nivelest_rg');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php
                    if( $objNivelEst->AdmtblDatosPersonales->id_status != 2 ){// diferente de BAJA
                    ?>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los cambios realizados...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <?php
                    }
                    ?>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la modificaci�n de datos...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 750px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Actualizaci�n : Nivel de Estudios</span>
            <fieldset class="fsetForm-Data" style="width: auto;">                                
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="cbxNivelEstudios">Nivel de Estudios:</label></td>
                        <td class="validation">
                            <select name="cbxNivelEstudios" id="cbxNivelEstudios">
                                <?php
                                echo $objCatNivelEst->shwNivelEstudios($objNivelEst->id_nivel_estudios);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                            
                    <tr style="height: 35px;">
                        <td><label>Eficiencia Terminal:</label></td>
                        <td class="validation">
                            <?php
                            if ($objNivelEst->eficencia_term == 1) {
                                $rbnEfTerm1 = 'checked="true"';
                                $rbnEfTerm2 = '';
                                $rbnEfTerm3 = '';
                            } else if ($objNivelEst->eficencia_term == 2) {
                                $rbnEfTerm1 = '';
                                $rbnEfTerm2 = 'checked="true"';
                                $rbnEfTerm3 = '';
                            } else if ($objNivelEst->eficencia_term == 3) {
                                $rbnEfTerm1 = '';
                                $rbnEfTerm2 = '';
                                $rbnEfTerm3 = 'checked="true"';
                            }  
                            ?>
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="1" <?php echo $rbnEfTerm1;?> />Concluido</label>
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerminal" value="2" <?php echo $rbnEfTerm2;?> />Cursando</label>
                            <label class="label-Radio"><input type="radio" name="rbnEfiTerminal" value="3" <?php echo $rbnEfTerm3;?> />Truncado</label>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtInstitucion">Instituci�n:</label></td>
                        <td class="validation">
                            <input type="text" name="txtInstitucion" id="txtInstitucion" value="<?php echo $objNivelEst->institucion;?>" maxlength="100" title="..." style="width: 400px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCct">C.C.T.:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCct" id="txtCct" value="<?php echo $objNivelEst->cct;?>" maxlength="15" title="..." style="width: 170px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCarrera">Carrera:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCarrera" id="txtCarrera" value="<?php echo $objNivelEst->carrera;?>" maxlength="100" title="..." style="width: 400px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtEspecialidad">Especialidad:</label></td>
                        <td class="validation">
                            <input type="text" name="txtEspecialidad" id="txtEspecialidad" value="<?php echo $objNivelEst->especialidad;?>" maxlength="100" title="..." style="width: 400px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtDocumento">Documento que avala:</label></td>
                        <td class="validation">
                            <input type="text" name="txtDocumento" id="txtDocumento" value="<?php echo $objNivelEst->documento;?>" maxlength="45" title="..." style="width: 250px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFolioDocto">Folio del documento:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFolioDocto" id="txtFolioDocto" value="<?php echo $objNivelEst->folio;?>" maxlength="10" title="..." style="width: 80px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCedula">No. de C�dula:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCedula" id="txtCedula" value="<?php echo $objNivelEst->n_cedula;?>" maxlength="7" title="..." style="width: 80px;" />
                        </td>
                    </tr>
                </table>
            </fieldset>           
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="2" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>