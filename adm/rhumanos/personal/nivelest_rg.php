<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_nivel_estudios.class.php';
$objNivelEst = new AdmtblNivelEstudios();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/datosrg.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Actualización');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    $curp = $_SESSION['xCurp'];
    // Nivel de Estudios
    $objNivelEst->curp = $curp;
    $objNivelEst->id_nivel_estudios = $_POST['cbxNivelEstudios'];
    $objNivelEst->eficencia_term = (isset($_POST['rbnEfiTerminal'])) ? $_POST['rbnEfiTerminal'] : 1;
    $objNivelEst->institucion = $_POST['txtInstitucion'];
    $objNivelEst->cct = $_POST['txtCct'];
    $objNivelEst->carrera = $_POST['txtCarrera'];
    $objNivelEst->especialidad = $_POST['txtEspecialidad'];    
    $objNivelEst->documento = $_POST['txtDocumento'];
    $objNivelEst->folio = $_POST['txtFolioDocto'];
    $objNivelEst->n_cedula = $_POST['txtCedula'];
    $objNivelEst->promedio = null;
           
    //-------------------------------------------------------------------//            
    if ($objNivelEst->update()) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_nivel_estudios', $curp, "Edt");
    } else {        
        $error = (!empty($objNivelEst->msjError)) ? $objNivelEst->msjError : 'Error al guardar los datos del Nivel de Estudios';        
    }
    
    $mod = $objSys->encrypt("nivelest_edit"); //(empty($error)) ? $objSys->encrypt("persona_menu") : $objSys->encrypt("nivelest_edit"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>