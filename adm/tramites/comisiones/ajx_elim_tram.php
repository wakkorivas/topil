<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_cambios_temporales.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario(); 
    
    $objCambiosTemp = new AdmtblCambiosTemporales();
    // Datos del tramite
    $objCambiosTemp->idcambio_temp = $_POST["id_tramite"];
    $objCambiosTemp->curp      = $_POST["curp"];                 

    $result = $objCambiosTemp->delete();
    if ( $result ) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_cambios_temporales', $objCambiosTemp->idcambio_temp, "Dlt");
        $ajx_datos['rslt']  = true; 
            
    } else {
        $ajx_datos['rslt']  = false;                     
        $ajx_datos['error'] = $objCambiosTemp->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>