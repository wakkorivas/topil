<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admcat_areas.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';
    include $path . 'includes/class/admtbl_cambios_temporales.class.php';
    include $path . 'includes/class/admcat_tipo_cambio.class.php';
    include $path . 'includes/class/admcat_municipios.class.php';
    include $path . 'includes/class/admcat_ubicaciones.class.php';
    //include $path . 'includes/class/logtbl_arm_armamento.class.php';
    //include $path . 'includes/class/logtbl_arm_asignacion.class.php';       
    $objSys = new System();
    $objAdscrip = new AdmtblAdscripcion();
    $objCambiosTemp = new AdmtblCambiosTemporales();
    $objArea = new AdmcatAreas();
    $objTipoCamb = new AdmcatTipoCambio();
    $objMunicipio = new AdmcatMunicipios();
    $objUbicacion = new AdmcatUbicaciones();
    //$objArm             = new LogtblArmArmamento();
    //$objAsigna          = new LogtblArmAsignacion();
    
    
    //se incluye archivo javascript para funcionalidad necesario
    $html .= '<script type="text/javascript" src="adm/_js/tramites/comisiones/ajx_frm_asigna.js"></script>';
    $html .= '<script type="text/javascript" src="includes/js/xgrid.js"></script>';
    
    $oper = $_POST["oper"];
    //variables recibidas
    $curp = $_POST["curp"];
    $id_tramite = $_POST["id_tramite"];
   
    if($oper == 0){   
        $objAdscrip->select($curp);
        $area = $objAdscrip->id_area;
        $municipio = $objAdscrip->id_municipio;
        $ubicacion = $objAdscrip->id_ubicacion;
        $tipo = 0;
        $fecha_inicio = "";
        $fecha_fin = "";
        $fecha_oficio = "";
        $fecha_soporte = "";
        
        
    }
    else{
        
        $objCambiosTemp->select($id_tramite);
        $area = $objCambiosTemp->id_area;
        $curp = $objCambiosTemp->curp;
        $municipio = $objCambiosTemp->id_municipio;
        $ubicacion = $objCambiosTemp->id_ubicacion;
        $tipo = $objCambiosTemp->id_tipo_cambio;
        $fecha_inicio = ($objCambiosTemp->fecha_inicio == NULL) ? "" : date('d/m/Y', strtotime($objCambiosTemp->fecha_inicio));
        $fecha_fin = ($objCambiosTemp->fecha_fin == NULL) ? "" : date('d/m/Y', strtotime($objCambiosTemp->fecha_fin));
        $fecha_oficio = ($objCambiosTemp->fecha_oficio == NULL) ? "" : date('d/m/Y', strtotime($objCambiosTemp->fecha_oficio));
        $fecha_soporte = ($objCambiosTemp->fecha_soporte == NULL) ? "" : date('d/m/Y', strtotime($objCambiosTemp->fecha_soporte));
    }
        
     

    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
    $html .= ' 
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <!--<span class="dvForm-Data-pTitle"><img src="'.PATH_IMAGES.'icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Nuevo</span>-->                                
            <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
                <ul>
                    <li><a href="#tab-1" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">1</span>Datos Generales</a></li>
                    <li><a href="#tab-2" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">2</span>Datos del Oficio</a></li>
                  
                </ul>         
                <!-- Datos Generales -->
                <div id="tab-1">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                           
                            <tr>
                                <td><label for="txtFechaInicio">Fecha de Inicio:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaInicio" id="txtFechaInicio" value="'.$fecha_inicio.'" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtFechaFin">Fecha de Finalizaci�n:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaFin" id="txtFechaFin" value="'.$fecha_fin.'" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    
                                </td>
                            </tr>     
                            <tr>
                                <td class="validation" colspan="2" style="border-bottom: 1px dotted gray; padding-bottom: 12px;">
                                    <label for="cbxArea">Lugar de Comisi�n:</label><br />
                                    <select name="cbxArea" id="cbxArea" style="max-width: 600px;">                                
                                        '.                                
                                         $objArea->shwAreas(70, $area)
                                         
                                         .'                                
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>   
                             <tr>
                                <td><label for="cbxRegion">Regi�n:</label></td>
                                <td class="validation">
                                    <select name="cbxRegion" id="cbxRegion" style="max-width: 500px;">
                                        '. $objMunicipio->select($municipio). $objMunicipio->AdmcatRegiones->shwRegiones($objMunicipio->id_region)
                                          
                                        .'
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxMpioAdscripcion">Municipio:</label></td>
                                <td class="validation">
                                    <select name="cbxMpioAdscripcion" id="cbxMpioAdscripcion" style="max-width: 500px;">
                                        '.
                                        $objMunicipio->shwMunicipios($municipio, 0, $objMunicipio->id_region)
                                        
                                        .'
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cbxUbicacion">Ubicaci�n:</label></td>
                                <td class="validation">
                                    <select name="cbxUbicacion" id="cbxUbicacion" style="max-width: 500px;">
                                       '.$objUbicacion->shwUbicaciones($ubicacion).'
                                     </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtTipoCamb">Tipo de Comisi�n:</label></td>
                                <td class="validation">
                                    <select name="cbxTipoCamb" id="cbxTipoCamb" style="max-width: 200px;">                                
                                         '. $objTipoCamb->shwTipoCambio($tipo,2).'
                                                                              
                                    </select>
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <script> 
                    $("#tabs").tabs (); 
                    $("#txtFechaInicio").datepicker({ yearRange: "1920:", }); 
                    $("#txtFechaFin").datepicker({ yearRange: "1920:", });
                    $("#txtFechaOficio").datepicker({ yearRange: "1920:", });
                    $("#txtFechaSoporte").datepicker({ yearRange: "1920:", });
                </script>
                <div id="tab-2">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtNoOficio">No. Oficio:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoOficio" id="txtNoOficio" value="'.$objCambiosTemp->no_oficio.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="txtFechaOficio">Fecha Oficio:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaOficio" id="txtFechaOficio" value="'. $fecha_oficio . '"  maxlength="35" title="..." style="width: 120px;" />
                                </td>
                            </tr>
                             <tr>
                                <td><label for="txtNoSoporte">No. de Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoSoporte" id="txtNoSoporte" value="'.$objCambiosTemp->no_soporte.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="txtFechaSoporte">Fecha de Soporte:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaSoporte" id="txtFechaSoporte" value="'. $fecha_soporte . '"  maxlength="35" title="..." style="width: 120px;" />
                                    
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFirmante">Firmante del Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtFirmante" id="txtFirmante" value="'.$objCambiosTemp->firmante_soporte.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                   
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtCargoFirmante">Cargo del Firmante:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtCargoFirmante" id="txtCargoFirmante" value="'.$objCambiosTemp->cargo_firmante.'"  maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                    
                                </td>
                            </tr>     
                            
                        </table>
                    </fieldset>
                </div>
                
            </div>
            
        </div>
         <input type="hidden" id="dtTypeOper" name="dtTypeOper" value="' . $oper . '" />
        <input type="hidden" id="id_tramite" name="id_tramite" value="' . $id_tramite . '" />
        <input type="hidden" id="curp" name="curp" value="' . $objSys->encrypt( $curp ) . '" />
    </form>     
   
    ';    
                  
   
  
    
    //se inyecta la informacion en los contenedores
    $ajx_datos["html"]  = utf8_encode($html);      
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>