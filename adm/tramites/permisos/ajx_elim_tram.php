
<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_permisos.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario(); 
    
    $objPermisos = new AdmtblPermisos();
    // Datos del tramite
    $objPermisos->id_permisos = $_POST["id_tramite"];
    $objPermisos->curp      = $_POST["curp"];                 

    $result = $objPermisos->delete();
    if ( $result ) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_tramites', $objPermisos->id_permisos, "Dlt");
        $ajx_datos['rslt']  = true; 
            
    } else {
        $ajx_datos['rslt']  = false;                     
        $ajx_datos['error'] = $objPermisos->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>