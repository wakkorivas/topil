<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_vacaciones.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario(); 
    
    $objVacaciones = new AdmtblVacaciones();
    // Datos del tramite
    $id_tramite = $_POST["id_tramite"];
    $param_tramites = split ("-", $id_tramite); 
    $objVacaciones->curp = $param_tramites[0];
    $objVacaciones->anio = $param_tramites[1];
    $objVacaciones->periodo = $param_tramites[2];
    
    $result = $objVacaciones->delete();
    if ( $result ) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_vacaciones', $id_tramite, "Dlt");
        $ajx_datos['error'] = $objVacaciones->msjError;
        $ajx_datos['rslt']  = true; 
            
    } else {
        $ajx_datos['rslt']  = false;                     
        $ajx_datos['error'] = $objVacaciones->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>