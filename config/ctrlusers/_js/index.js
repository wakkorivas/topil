var xGrid = "",
    hWind = 0;
$(document).ready(function(){
    hWind = $(window).height();
    //-- Ajusta el tamaño del contenedor del grid...
    $('#dvGrid-Usrs').css('height', (hWind - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Usrs').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xRowsDisplay: 35,
        xTypeDataAjax: 'json'
    });
        
    
    $('#dvGrid-Usrs a.lnkBtnOpcionGrid').live('click', function(){
        var this_obj = $(this);
        if( this_obj.data('btn') == 'dlt' ){
            dltUser( this_obj.data('id') );
        }
    });
    
});

/*
* Función para dar de baja un usuario.
*/
function dltUser(xId){
    var frmPreg = $(getHTMLMensaje('¿Está seguro de inactivar el usuario seleccionado?', 2));
    frmPreg.dialog({
        autoOpen: true,
        modal: true,
        resizable: false,
        width: 400,
        buttons:{
            'Aceptar': function(){
                // Se genera el diálogo de espera
                var frmWait = $('<div><div class="xGrid-dvWait-LoadData">Procesando la solicitud, por favor espere...</div></div>');
                // Se procesan los datos vía ajax
                $.ajax({
                    url: xDcrypt($('#hdnUrlEditUsr').val()),
                    data: {
                        'oper': 'delete',
                        'id': xId,
                    },
                    type: 'post',
                    dataType: 'json',
                    async: true,
                    cache: false,
                    beforeSend: function () {
                        frmPreg.dialog('close');
                        // Díalogo de espera                     
                        frmWait.dialog({
                            autoOpen: true,
                            modal: true,
                            width: 400,
                        }); 
                    },
                    success: function (xdata) {
                        // Cierra el diálogo de espera
                        frmWait.dialog('close');
                        // Verifica el resultado del proceso
                        if (xdata.rslt) {
                            xGrid.refreshGrid();
                        } else {
                            shwError('Error: ' + xdata.error);
                        }
                    },
                    error: function(objeto, detalle, otroobj){
                        frmWait.dialog('close');
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }
                });
            },
            'Cancelar': function(){
                frmPreg.dialog('close');
            }
        }
    });
}