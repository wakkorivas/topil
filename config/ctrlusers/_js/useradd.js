$(document).ready(function(){
    // Inicialización de los componentes Fecha
    /*
    $('#txtFechaIng').datepicker({
        yearRange: '1950:',
    });
    */

    // Convierte a mayúsculas el contenido de los textbox y textarea    
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtNombreUsr').blur(function(){ $(this).val(xTrim( $(this).val().toLowerCase() )) });
        
    // Aplicación de las reglas de validación de los campos del formulario   
    $('#frmRegistro').validate({
        rules:{
            // Adscripción
            txtNombre: 'required',
            txtNombreUsr: 'required',
            txtPswd: 'required',
            txtPswd2: {
                required: true,
                equalTo: "#txtPswd",
            },
            txtFiltroIP: 'required',
    	},
    	messages:{
            // Adscripción
            txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtNombreUsr: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtPswd: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtPswd2:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                equalTo: '<span class="ValidateError" title="Confirmaci&oacute;n de contrase&ntilde;a incorrecta"></span>'
            },
            txtFiltroIP: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    // Acción del botón Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('¿Está seguro de continuar con la alta del nuevo usuario?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 390,
                buttons:{
                    'Aceptar': function(){
                        $('#frmRegistro').submit();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });            
        }
    });  
    
});