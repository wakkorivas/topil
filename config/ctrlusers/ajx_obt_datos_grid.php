<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de parámetros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una búsqueda.
 * @param int colSort, contiene el índice de la columna por la cual se ordenarán los datos.
 * @param string typeSort, define el tipo de ordenación de los datos: ASC o DESC.
 * @param int rowStart, especifica el número de fila por la cual inicia la paginación del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrarán en cada página del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=utf-8');
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/xtblusuarios.class.php';
    $objSys = new System();
    $objUsers = new Xtblusuarios();

    //--------------------- Recepción de parámetros --------------------------//
    // Búsqueda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where = 'a.nom_usr LIKE ? '
                   . 'OR a.nombre LIKE ? '
                   . 'OR b.perfil LIKE ? ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',);
    }
    // Ordenación...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'a.id_usuario ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'a.nom_usr' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'a.nombre ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'b.perfil ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'a.fecha_reg ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'a.ultimo_acceso ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 7) {
        $sql_order = 'a.stat ' . $_GET['typeSort'];
    }   
    // Paginación...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
       
    $datos = $objUsers->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["id_usuario"]);
            $sty_color = ( $dato["stat"] == 'INACTIVO' ) ? 'color: #a09f9d;' : '';
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_usuario"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 3%;">';
                   $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 7%; ' . $sty_color . '">' . $dato["id_usuario"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '"><a href="#" class="toolTipTrigger" rel="' . $id_crypt . '" style=" ' . $sty_color . '">' . $dato['nom_usr'] . '</a></td>';
                $html .= '<td style="text-align: left; width: 20%; ' . $sty_color . '">' . $dato["nombre"] . '</td>';                                
                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">' . $dato["perfil"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">' . $dato["fecha_reg"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">' . $dato["ultimo_acceso"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">' . $dato["stat"] . '</td>';
                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("ctrl_user") . "&id=" . $id_crypt;
                $url_audit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("user_audit") . "&id=" . $id_crypt;
                $html .= '<td style="text-align: center; width: 15%;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Modificar usuario..." ><img src="' . PATH_IMAGES . 'icons/editar24.png" alt="modificar" /></a>';                
                //$html .= '  <a href="' . $url_audit . '" class="lnkBtnOpcionGrid" title="Auditoría del usuario..." ><img src="' . PATH_IMAGES . 'icons/historial.png" alt="auditoria" /></a>';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" data-id="' . $dato["id_usuario"] . '" data-btn="dlt" title="Baja del usuario..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';  		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron usuarios en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = $html;
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesión...";
}
?>