<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//


//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Configuración',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="config/ctrlusers/_js/index.js?v=1.0"></script>'),
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opción... -->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('user_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo usuario...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>
                    
                    <a href="#" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al índice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Usrs" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 7%;" class="xGrid-tbCols-ColSortable">ID</th>  
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">USUARIO</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">PERFIL</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">FECHA ALTA</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ÚLTIMO ACCESO</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">STATUS</th>
                    <th style="width: 15%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <div id="dvForm-Ficha" title="SISP :: Ficha de información">
        
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlEditUsr" value="<?php echo $objSys->encrypt('config/ctrlusers/ajx_upd_usr.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>