<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/xcatperfiles.class.php';
$objCatPerf = new Xcatperfiles();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Configuración',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="config/ctrlusers/_js/useradd.js?v=1.0"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('user_rg');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo usuario...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del usuario...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Usr" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 750px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Configuración : Nuevo Usuario</span>
            <fieldset class="fsetForm-Data" style="width: auto;">                                
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="txtNombre">Nombre Completo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombre" id="txtNombre" value="" maxlength="150" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNombreUsr">Nombre de Usuario:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombreUsr" id="txtNombreUsr" value="" maxlength="30" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPswd">Contraseña:</label></td>
                        <td class="validation">
                            <input type="password" name="txtPswd" id="txtPswd" value="" maxlength="30" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPswd2">Confirmar Contraseña:</label></td>
                        <td class="validation">
                            <input type="password" name="txtPswd2" id="txtPswd2" value="" maxlength="30" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxPerfil">Perfil General:</label></td>
                        <td class="validation">
                            <select name="cbxPerfil" id="cbxPerfil">
                                <option value="0">- Perfiles -</option>
                                <?php
                                echo $objCatPerf->shwPerfiles();
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFiltroIP">Filtro IP:</label></td>
                        <td class="validation">
                            <textarea name="txtFiltroIP" id="txtFiltroIP" title="..." style="heigth: 70px; width: 180px;" placeholder="x.x.x.x - Sin restricción" /></textarea>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                    
                </table>
            </fieldset>           
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podrá continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="1" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>