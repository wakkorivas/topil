<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/xcatperfiles.class.php';
include 'includes/class/xtblusuarios.class.php';
$objCatPerf = new Xcatperfiles();
$objUsers = new Xtblusuarios();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Configuración',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="config/ctrlusers/_js/useradd.js?v=1.0"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
    $urlBack = "index.php?m=" . $_SESSION["xIdMenu"];
    
    $_SESSION["xIdUsuario"] = $objSys->decrypt($_GET['id']);
    $objUsers->select($_SESSION["xIdUsuario"]);
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <!--
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo usuario...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    -->
                    <a href="<?php echo $urlBack?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Regresar al listado general de usuarios...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <input type="hidden" name="dtIdUsr" id="dtIdUsr" value="<?php echo $objUsers->id_usuario?>" />
        <div id="dvForm-Usr" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 750px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Configuración : Nuevo Usuario</span>
            <fieldset class="fsetForm-Data" style="width: auto;">                                
                <table class="tbForm-Data">
                    <tr>
                        <td><label for="txtNombre">Nombre Completo:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombre" id="txtNombre" value="<?php echo $objUsers->nombre;?>" maxlength="150" title="..." style="width: 350px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNombreUsr">Nombre de Usuario:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombreUsr" id="txtNombreUsr" value="<?php echo $objUsers->nom_usr;?>" maxlength="30" title="..." readonly="true" style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPswd">Contraseña:</label></td>
                        <td class="validation">
                            <input type="password" name="txtPswd" id="txtPswd" value="**********" maxlength="30" title="..." readonly="true" style="width: 200px;" />
                            <span class="pRequerido">*</span>
                            <a href="#" class="lnkBtnOpcionGrid" style="min-height: 18px; min-width: 18px;">
                                <img src="<?php echo PATH_IMAGES;?>icons/edit16.png" alt="edit_pswd" title="Modificar contraseña" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPswd2">Confirmar Contraseña:</label></td>
                        <td class="validation">
                            <input type="password" name="txtPswd2" id="txtPswd2" value="**********" maxlength="30" title="..." readonly="true" style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxPerfil">Perfil General:</label></td>
                        <td class="validation">
                            <select name="cbxPerfil" id="cbxPerfil">
                                <option value="0">- Perfiles -</option>
                                <?php
                                echo $objCatPerf->shwPerfiles($objUsers->id_perfil);
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFiltroIP">Filtro IP:</label></td>
                        <td class="validation">
                            <textarea name="txtFiltroIP" id="txtFiltroIP" title="..." style="heigth: 60px; width: 180px;"><?php echo $objUsers->filtro_ip;?></textarea>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>                    
                </table>
            </fieldset>           
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podrá continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="2" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>