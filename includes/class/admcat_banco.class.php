<?php
/**
 *
 */
class AdmcatBanco
{
    public $id_banco; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $banco; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de los bancos dentro de un combobox.
     * @param int $id, id del banco seleccionado por deafult     
     * @return array html(options)
     */
    public function shwBancos($id=0){
        $aryDatos = $this->selectAll('', 'banco Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_banco"] )
                $html .= '<option value="'.$datos["id_banco"].'" selected>'.$datos["banco"].'</option>';
            else
                $html .= '<option value="'.$datos["id_banco"].'" >'.$datos["banco"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_banco)
    {
        $sql = "SELECT id_banco, banco
                FROM admcat_banco
                WHERE id_banco=:id_banco;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_banco' => $id_banco));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_banco = $data['id_banco'];
            $this->banco = $data['banco'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_banco, a.banco
                FROM admcat_banco a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_banco' => $data['id_banco'],
                               'banco' => $data['banco'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_banco(id_banco, banco)
                VALUES(:id_banco, :banco);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_banco" => $this->id_banco, ":banco" => $this->banco));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_banco
                   SET banco=:banco
                WHERE id_banco=:id_banco;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_banco" => $this->id_banco, ":banco" => $this->banco));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>