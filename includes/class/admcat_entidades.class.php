<?php
/**
 *
 */
class AdmcatEntidades
{
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $entidad; /** @Tipo: varchar(150), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $abreviacion; /** @Tipo: varchar(5), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_pais; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatPaises; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_paises.class.php';
        $this->AdmcatPaises = new AdmcatPaises();
    }

    /**
     * Funci�n para mostrar la lista de entidades dentro de un combobox.
     * @param int $id, id de la entidad seleccionada por deafult  
     * @param int $id_pais, id del pa�s para el filtro de entidades
     * @return array html(options)
     */
    public function shwEntidades($id=0, $id_pais=0){
        $aryDatos = $this->selectAll('a.id_pais=' . $id_pais, 'a.entidad Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_entidad"] )
                $html .= '<option value="'.$datos["id_entidad"].'" selected>'.$datos["entidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_entidad"].'" >'.$datos["entidad"].'</option>';
        }
        return $html;
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_entidad)
    {
        $sql = "SELECT id_entidad, entidad, abreviacion, id_pais
                FROM admcat_entidades
                WHERE id_entidad=:id_entidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_entidad' => $id_entidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_entidad = $data['id_entidad'];
            $this->entidad = $data['entidad'];
            $this->abreviacion = $data['abreviacion'];
            $this->id_pais = $data['id_pais'];

            $this->AdmcatPaises->select($this->id_pais);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_entidad, a.entidad, a.abreviacion, a.id_pais, b.pais
                FROM admcat_entidades a 
                 LEFT JOIN admcat_paises b ON a.id_pais=b.id_pais";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_entidad' => $data['id_entidad'],
                               'entidad' => $data['entidad'],
                               'abreviacion' => $data['abreviacion'],
                               'id_pais' => $data['id_pais'],
                               'pais' => $data['pais'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_entidades(id_entidad, entidad, abreviacion, id_pais)
                VALUES(:id_entidad, :entidad, :abreviacion, :id_pais);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_entidad" => $this->id_entidad, ":entidad" => $this->entidad, ":abreviacion" => $this->abreviacion, ":id_pais" => $this->id_pais));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_entidades
                   SET entidad=:entidad, abreviacion=:abreviacion, id_pais=:id_pais
                WHERE id_entidad=:id_entidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_entidad" => $this->id_entidad, ":entidad" => $this->entidad, ":abreviacion" => $this->abreviacion, ":id_pais" => $this->id_pais));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }   
}


?>