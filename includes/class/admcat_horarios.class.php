<?php
/**
 *
 */
class AdmcatHorarios
{
    public $id_horario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $horario; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de los horarios dentro de un combobox.
     * @param int $id, id del horario seleccionado por deafult     
     * @return array html(options)
     */
    public function shwHorarios($id=0){
        $aryDatos = $this->selectAll('', 'id_horario Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_horario"] )
                $html .= '<option value="'.$datos["id_horario"].'" selected>'.$datos["horario"].'</option>';
            else
                $html .= '<option value="'.$datos["id_horario"].'" >'.$datos["horario"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_horario)
    {
        $sql = "SELECT id_horario, horario, status
                FROM admcat_horarios
                WHERE id_horario=:id_horario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_horario' => $id_horario));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_horario = $data['id_horario'];
            $this->horario = $data['horario'];
            $this->status = $data['status'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_horario, a.horario, a.status
                FROM admcat_horarios a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_horario' => $data['id_horario'],
                               'horario' => $data['horario'],
                               'status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_horarios(id_horario, horario, status)
                VALUES(:id_horario, :horario, :status);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_horario" => $this->id_horario, ":horario" => $this->horario, ":status" => $this->status));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_horarios
                   SET horario=:horario, status=:status
                WHERE id_horario=:id_horario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_horario" => $this->id_horario, ":horario" => $this->horario, ":status" => $this->status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>