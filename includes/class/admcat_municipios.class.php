<?php
/**
 *
 */
class AdmcatMunicipios
{
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $municipio; /** @Tipo: varchar(150), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cabecera; /** @Tipo: varchar(150), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_region; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatRegiones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatEntidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_regiones.class.php';
        require_once 'admcat_entidades.class.php';
        $this->AdmcatRegiones = new AdmcatRegiones();
        $this->AdmcatEntidades = new AdmcatEntidades();
    }

    /**
     * Funci�n para mostrar la lista de municipios dentro de un combobox.
     * @param int $id, id del municipio seleccionado por deafult  
     * @param int $id_entidad, id de la entidad para el filtro de municipios
     * @return array html(options)
     */
    public function shwMunicipios($id=0, $id_entidad=0, $id_region=0){
        if ($id_entidad != 0)
            $aryDatos = $this->selectAll('a.id_entidad=' . $id_entidad, 'a.municipio Asc');
        else if ($id_region != 0)
            $aryDatos = $this->selectAll('a.id_region=' . $id_region, 'a.municipio Asc');
        else
            $aryDatos = $this->selectAll('', 'a.municipio Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_municipio"] )
                $html .= '<option value="'.$datos["id_municipio"].'" selected>'.$datos["municipio"].'</option>';
            else
                $html .= '<option value="'.$datos["id_municipio"].'" >'.$datos["municipio"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_municipio)
    {
        $sql = "SELECT id_municipio, municipio, cabecera, id_entidad, id_region
                FROM admcat_municipios
                WHERE id_municipio=:id_municipio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_municipio' => $id_municipio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_municipio = $data['id_municipio'];
            $this->municipio = $data['municipio'];
            $this->cabecera = $data['cabecera'];
            $this->id_entidad = $data['id_entidad'];
            $this->id_region = $data['id_region'];

            $this->AdmcatEntidades->select($this->id_entidad);
            $this->AdmcatRegiones->select($this->id_region);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_municipio, a.municipio, a.cabecera, a.id_entidad, a.id_region, b.region, c.entidad, c.abreviacion, c.id_pais
                FROM admcat_municipios a 
                 LEFT JOIN admcat_regiones b ON a.id_region=b.id_region
                 LEFT JOIN admcat_entidades c ON a.id_entidad=c.id_entidad";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_municipio' => $data['id_municipio'],
                               'municipio' => $data['municipio'],
                               'cabecera' => $data['cabecera'],
                               'id_entidad' => $data['id_entidad'],
                               'id_region' => $data['id_region'],
                               'region' => $data['region'],
                               'entidad' => $data['entidad'],
                               'abreviacion' => $data['abreviacion'],
                               'id_pais' => $data['id_pais'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_municipios(id_municipio, municipio, cabecera, id_entidad, id_region)
                VALUES(:id_municipio, :municipio, :cabecera, :id_entidad, :id_region);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":municipio" => $this->municipio, ":cabecera" => $this->cabecera, ":id_entidad" => $this->id_entidad, ":id_region" => $this->id_region));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_municipios
                   SET municipio=:municipio, cabecera=:cabecera, id_region=:id_region
                WHERE id_municipio=:id_municipio AND id_entidad=:id_entidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":municipio" => $this->municipio, ":cabecera" => $this->cabecera, ":id_entidad" => $this->id_entidad, ":id_region" => $this->id_region));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }   
}


?>