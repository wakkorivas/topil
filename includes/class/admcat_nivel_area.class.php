<?php
/**
 *
 */
class AdmcatNivelArea
{
    public $id_nivel; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nivel; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $gerarquia; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_nivel)
    {
        $sql = "SELECT id_nivel, nivel, gerarquia
                FROM admcat_nivel_area
                WHERE id_nivel=:id_nivel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_nivel' => $id_nivel));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_nivel = $data['id_nivel'];
            $this->nivel = $data['nivel'];
            $this->gerarquia = $data['gerarquia'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_nivel, a.nivel, a.gerarquia
                FROM admcat_nivel_area a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_nivel' => $data['id_nivel'],
                               'nivel' => $data['nivel'],
                               'gerarquia' => $data['gerarquia'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_nivel_area(id_nivel, nivel, gerarquia)
                VALUES(:id_nivel, :nivel, :gerarquia);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_nivel" => $this->id_nivel, ":nivel" => $this->nivel, ":gerarquia" => $this->gerarquia));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_nivel_area
                   SET nivel=:nivel, gerarquia=:gerarquia
                WHERE id_nivel=:id_nivel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_nivel" => $this->id_nivel, ":nivel" => $this->nivel, ":gerarquia" => $this->gerarquia));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>