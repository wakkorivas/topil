<?php
/**
 *
 */
class AdmcatOperativosSeguridad
{
    public $id_operativo_seg; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nombre_operativo; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $Objetivo; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $Observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_inicio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_operativo_seg)
    {
        $sql = "SELECT id_operativo_seg, nombre_operativo, Objetivo, Observaciones, fecha_inicio, fecha_fin
                FROM admcat_operativos_seguridad
                WHERE id_operativo_seg=:id_operativo_seg;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_operativo_seg' => $id_operativo_seg));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_operativo_seg = $data['id_operativo_seg'];
            $this->nombre_operativo = $data['nombre_operativo'];
            $this->Objetivo = $data['Objetivo'];
            $this->Observaciones = $data['Observaciones'];
            $this->fecha_inicio = $data['fecha_inicio'];
            $this->fecha_fin = $data['fecha_fin'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_operativo_seg, a.nombre_operativo, a.Objetivo, a.Observaciones, a.fecha_inicio, a.fecha_fin
                FROM admcat_operativos_seguridad a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_operativo_seg' => $data['id_operativo_seg'],
                               'nombre_operativo' => $data['nombre_operativo'],
                               'Objetivo' => $data['Objetivo'],
                               'Observaciones' => $data['Observaciones'],
                               'fecha_inicio' => $data['fecha_inicio'],
                               'fecha_fin' => $data['fecha_fin'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_operativos_seguridad(id_operativo_seg, nombre_operativo, Objetivo, Observaciones, fecha_inicio, fecha_fin)
                VALUES(:id_operativo_seg, :nombre_operativo, :Objetivo, :Observaciones, :fecha_inicio, :fecha_fin);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_operativo_seg" => $this->id_operativo_seg, ":nombre_operativo" => $this->nombre_operativo, ":Objetivo" => $this->Objetivo, ":Observaciones" => $this->Observaciones, ":fecha_inicio" => $this->fecha_inicio, ":fecha_fin" => $this->fecha_fin));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_operativos_seguridad
                   SET nombre_operativo=:nombre_operativo, Objetivo=:Objetivo, Observaciones=:Observaciones, fecha_inicio=:fecha_inicio, fecha_fin=:fecha_fin
                WHERE id_operativo_seg=:id_operativo_seg;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_operativo_seg" => $this->id_operativo_seg, ":nombre_operativo" => $this->nombre_operativo, ":Objetivo" => $this->Objetivo, ":Observaciones" => $this->Observaciones, ":fecha_inicio" => $this->fecha_inicio, ":fecha_fin" => $this->fecha_fin));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>