<?php
/**
 *
 */
class AdmcatTipoCurso
{
    public $id_tipo_curso; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_curso; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de los tipos de cursos dentro de un combobox.
     * @param int $id, id del tipo de curso seleccionado por deafult     
     * @return array html(options)
     */
    public function shwTipoCurso($id=0){
        $aryDatos = $this->selectAll('', 'id_tipo_curso Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_curso"] )
                $html .= '<option value="'.$datos["id_tipo_curso"].'" selected>'.$datos["tipo_curso"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_curso"].'" >'.$datos["tipo_curso"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_curso)
    {
        $sql = "SELECT id_tipo_curso, tipo_curso
                FROM admcat_tipo_curso
                WHERE id_tipo_curso=:id_tipo_curso;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_curso' => $id_tipo_curso));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_curso = $data['id_tipo_curso'];
            $this->tipo_curso = $data['tipo_curso'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_tipo_curso, a.tipo_curso
                FROM admcat_tipo_curso a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_curso' => $data['id_tipo_curso'],
                               'tipo_curso' => $data['tipo_curso'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_tipo_curso(id_tipo_curso, tipo_curso)
                VALUES(:id_tipo_curso, :tipo_curso);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_curso" => $this->id_tipo_curso, ":tipo_curso" => $this->tipo_curso));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_tipo_curso
                   SET tipo_curso=:tipo_curso
                WHERE id_tipo_curso=:id_tipo_curso;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_curso" => $this->id_tipo_curso, ":tipo_curso" => $this->tipo_curso));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>