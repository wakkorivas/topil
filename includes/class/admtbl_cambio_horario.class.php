<?php
/**
 *
 */
class AdmtblCambioHorario
{
    public $id_cambio_horario; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_reg; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_cambio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $notificar; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observacion; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_oficio; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_soporte; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_soporte; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $firmante_soporte; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cargo_firmante; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_horario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatHorarios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_horarios.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatHorarios = new AdmcatHorarios();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_cambio_horario)
    {
        $sql = "SELECT id_cambio_horario, fecha_reg, fecha_cambio, notificar, status, observacion, folio, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante, curp, id_horario
                FROM admtbl_cambio_horario
                WHERE id_cambio_horario=:id_cambio_horario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_cambio_horario' => $id_cambio_horario));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_cambio_horario = $data['id_cambio_horario'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->fecha_cambio = $data['fecha_cambio'];
            $this->notificar = $data['notificar'];
            $this->status = $data['status'];
            $this->observacion = $data['observacion'];
            $this->folio = $data['folio'];
            $this->no_oficio = $data['no_oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->no_soporte = $data['no_soporte'];
            $this->fecha_soporte = $data['fecha_soporte'];
            $this->firmante_soporte = $data['firmante_soporte'];
            $this->cargo_firmante = $data['cargo_firmante'];
            $this->curp = $data['curp'];
            $this->id_horario = $data['id_horario'];

            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatHorarios->select($this->id_horario);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    /**
     * Funci�n para seleccionar los datos  de la tabla admtbl_permisos para el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos 
    */
    public function selectListTramite($sqlWhere='', $sqlValues=array() )
    {
        $sql = "select id_cambio_horario, fecha_cambio,ch.id_horario, c.horario,  observacion, fecha_reg, folio  
                from admtbl_cambio_horario ch
                LEFT JOIN admcat_horarios c ON ch.id_horario=c.id_horario
                ";
        if (!empty($sqlWhere))
            $sql .= " WHERE  $sqlWhere";                
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            //echo $sql;
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                
                $datos[] = array(
                               'id_cambio_horario' => $data['id_cambio_horario'],
                               'fecha_cambio' => $data['fecha_cambio'],
                               'fecha_reg' => $data['fecha_reg'],
                               'observacion' => $data['observacion'],
                               'horario' => $data['horario'],
                               'folio' => $data['folio'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_cambio_horario, a.fecha_reg, a.fecha_cambio, a.notificar, a.status, a.observacion, a.folio, a.no_oficio, a.fecha_oficio, a.no_soporte, a.fecha_soporte, a.firmante_soporte, a.cargo_firmante, a.curp, a.id_horario,
                  b.curp, b.nombre, b.a_paterno, b.a_materno, b.fecha_nac, b.genero, b.rfc, b.cuip, b.folio_ife, b.mat_cartilla, b.licencia_conducir, b.pasaporte, b.id_estado_civil, b.id_tipo_sangre, b.id_nacionalidad, b.id_entidad, b.id_municipio, b.lugar_nac, b.id_status,
                  c.id_horario, c.horario, c.status
                FROM admtbl_cambio_horario a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp
                 LEFT JOIN admcat_horarios c ON a.id_horario=c.id_horario";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_cambio_horario' => $data['id_cambio_horario'],
                               'fecha_reg' => $data['fecha_reg'],
                               'fecha_cambio' => $data['fecha_cambio'],
                               'notificar' => $data['notificar'],
                               'status' => $data['status'],
                               'observacion' => $data['observacion'],
                               'folio' => $data['folio'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'no_soporte' => $data['no_soporte'],
                               'fecha_soporte' => $data['fecha_soporte'],
                               'firmante_soporte' => $data['firmante_soporte'],
                               'cargo_firmante' => $data['cargo_firmante'],
                               'curp' => $data['curp'],
                               'id_horario' => $data['id_horario'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               'admcat_horarios_horario' => $data['horario'],
                               'admcat_horarios_status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_cambio_horario( fecha_reg, fecha_cambio, notificar, status, observacion, folio, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante, curp, id_horario)
                VALUES(:fecha_reg, :fecha_cambio, :notificar, :status, :observacion, :folio, :no_oficio, :fecha_oficio, :no_soporte, :fecha_soporte, :firmante_soporte, :cargo_firmante, :curp, :id_horario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array( ":fecha_reg" => date("Y-m-d"), ":fecha_cambio" => $this->fecha_cambio, ":notificar" => $this->notificar, ":status" => $this->status, ":observacion" => $this->observacion, ":folio" => $this->folio, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante, ":curp" => $this->curp, ":id_horario" => $this->id_horario));
            if ($qry){
                $sql_upd = "UPDATE admtbl_adscripcion
                       SET id_horario=:id_horario 
                       WHERE curp=:curp;";
                try {
                    $qry_upd = $this->_conexBD->prepare($sql_upd);
                    $qry_upd->execute(array(":curp" => $this->curp, ":id_horario" => $this->id_horario ));
                    if ($qry_upd) 
                        return true;
                    else
                        return false;
                } catch(PDOException $e) {
                    $this->msjError = $e->getMessage();
                    return false;
                }
                    //return $this->_conexBD->lastInsertId();
            }
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_cambio_horario
                   SET fecha_reg=:fecha_reg, fecha_cambio=:fecha_cambio, notificar=:notificar, status=:status, observacion=:observacion, folio=:folio, no_oficio=:no_oficio, fecha_oficio=:fecha_oficio, no_soporte=:no_soporte, fecha_soporte=:fecha_soporte, firmante_soporte=:firmante_soporte, cargo_firmante=:cargo_firmante, curp=:curp, id_horario=:id_horario
                WHERE id_cambio_horario=:id_cambio_horario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cambio_horario" => $this->id_cambio_horario, ":fecha_reg" => $this->fecha_reg, ":fecha_cambio" => $this->fecha_cambio, ":notificar" => $this->notificar, ":status" => $this->status, ":observacion" => $this->observacion, ":folio" => $this->folio, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante, ":curp" => $this->curp, ":id_horario" => $this->id_horario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

  public function delete()
    {
        $sql = "DELETE FROM admtbl_cambio_horario
                WHERE id_cambio_horario=:id_cambio_horario 
                AND curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cambio_horario" => $this->id_cambio_horario, ":curp" => $this->curp));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }}


?>