<?php
/**
 *
 */
class AdmtblEvaluaciones
{
    public $id_evaluacion; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_eval; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_ini; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $evaluacion; /** @Tipo: varchar(150), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $importancia; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $duracion; /** @Tipo: varchar(15), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_result_eval; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatTipoEval; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatResultEval; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_tipo_eval.class.php';
        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_result_eval.class.php';
        $this->AdmcatTipoEval = new AdmcatTipoEval();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatResultEval = new AdmcatResultEval();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_evaluacion)
    {
        $sql = "SELECT id_evaluacion, curp, id_tipo_eval, fecha_ini, fecha_fin, evaluacion, importancia, duracion, id_result_eval, observaciones
                FROM admtbl_evaluaciones
                WHERE id_evaluacion=:id_evaluacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_evaluacion' => $id_evaluacion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_evaluacion = $data['id_evaluacion'];
            $this->curp = $data['curp'];
            $this->id_tipo_eval = $data['id_tipo_eval'];
            $this->fecha_ini = $data['fecha_ini'];
            $this->fecha_fin = $data['fecha_fin'];
            $this->evaluacion = $data['evaluacion'];
            $this->importancia = $data['importancia'];
            $this->duracion = $data['duracion'];
            $this->id_result_eval = $data['id_result_eval'];
            $this->observaciones = $data['observaciones'];

            $this->AdmcatTipoEval->select($this->id_tipo_eval);
            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatResultEval->select($this->id_result_eval);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_evaluacion, a.curp, a.id_tipo_eval, a.fecha_ini, a.fecha_fin, a.evaluacion, a.importancia, a.duracion, a.id_result_eval, a.observaciones,
                  b.tipo_eval,
                  c.resultado, c.aprobatorio
                FROM admtbl_evaluaciones a 
                 LEFT JOIN admcat_tipo_eval b ON a.id_tipo_eval=b.id_tipo_eval
                 LEFT JOIN admcat_result_eval c ON a.id_result_eval=c.id_result_eval";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_evaluacion' => $data['id_evaluacion'],
                               'curp' => $data['curp'],
                               'id_tipo_eval' => $data['id_tipo_eval'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'evaluacion' => $data['evaluacion'],
                               'importancia' => $data['importancia'],
                               'duracion' => $data['duracion'],
                               'id_result_eval' => $data['id_result_eval'],
                               'observaciones' => $data['observaciones'],
                               'tipo_eval' => $data['tipo_eval'],                               
                               'resultado' => $data['resultado'],
                               'aprobatorio' => $data['aprobatorio'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_evaluaciones(id_evaluacion, curp, id_tipo_eval, fecha_ini, fecha_fin, evaluacion, importancia, duracion, id_result_eval, observaciones)
                VALUES(:id_evaluacion, :curp, :id_tipo_eval, :fecha_ini, :fecha_fin, :evaluacion, :importancia, :duracion, :id_result_eval, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_evaluacion" => $this->id_evaluacion, ":curp" => $this->curp, ":id_tipo_eval" => $this->id_tipo_eval, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":evaluacion" => $this->evaluacion, ":importancia" => $this->importancia, ":duracion" => $this->duracion, ":id_result_eval" => $this->id_result_eval, ":observaciones" => $this->observaciones));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_evaluaciones
                   SET curp=:curp, id_tipo_eval=:id_tipo_eval, fecha_ini=:fecha_ini, fecha_fin=:fecha_fin, evaluacion=:evaluacion, importancia=:importancia, duracion=:duracion, id_result_eval=:id_result_eval, observaciones=:observaciones
                WHERE id_evaluacion=:id_evaluacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_evaluacion" => $this->id_evaluacion, ":curp" => $this->curp, ":id_tipo_eval" => $this->id_tipo_eval, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":evaluacion" => $this->evaluacion, ":importancia" => $this->importancia, ":duracion" => $this->duracion, ":id_result_eval" => $this->id_result_eval, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>