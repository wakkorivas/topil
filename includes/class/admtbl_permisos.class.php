<?php
/**
 *
 */
class AdmtblPermisos
{
    public $id_permisos; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_reg; /** @Tipo: datetime, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_ini; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $num_dias; /** @Tipo: tinyint(3), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $indefinida; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tipo_licencia; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $prorroga; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $motivo; /** @Tipo: varchar(200), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $opinion; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $titular; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tajeta; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $nombramiento; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $lugar_checa; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $comision; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $sueldo; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ofi_paga; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_ini_ant; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_fin_ant; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $num_dias_ant; /** @Tipo: tinyint(3), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tipo_licencia_ant; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $prorroga_ant; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_oficio; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_soporte; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_soporte; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $firmante_soporte; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cargo_firmante; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admtbl_datos_personales.class.php';
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }
    
        

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
      
    public function select($id_permisos)
    {
        $sql = "SELECT id_permisos, curp, fecha_reg, fecha_ini, fecha_fin, num_dias, indefinida, tipo_licencia, prorroga, motivo, opinion, titular, tajeta, nombramiento, lugar_checa, comision, sueldo, ofi_paga, fecha_ini_ant, fecha_fin_ant, num_dias_ant, tipo_licencia_ant, prorroga_ant, folio, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante
                FROM admtbl_permisos
                WHERE id_permisos=:id_permisos;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_permisos' => $id_permisos));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_permisos = $data['id_permisos'];
            $this->curp = $data['curp'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->fecha_ini = $data['fecha_ini'];
            $this->fecha_fin = $data['fecha_fin'];
            $this->num_dias = $data['num_dias'];
            $this->indefinida = $data['indefinida'];
            $this->tipo_licencia = $data['tipo_licencia'];
            $this->prorroga = $data['prorroga'];
            $this->motivo = $data['motivo'];
            $this->opinion = $data['opinion'];
            $this->titular = $data['titular'];
            $this->tajeta = $data['tajeta'];
            $this->nombramiento = $data['nombramiento'];
            $this->lugar_checa = $data['lugar_checa'];
            $this->comision = $data['comision'];
            $this->sueldo = $data['sueldo'];
            $this->ofi_paga = $data['ofi_paga'];
            $this->fecha_ini_ant = $data['fecha_ini_ant'];
            $this->fecha_fin_ant = $data['fecha_fin_ant'];
            $this->num_dias_ant = $data['num_dias_ant'];
            $this->tipo_licencia_ant = $data['tipo_licencia_ant'];
            $this->prorroga_ant = $data['prorroga_ant'];
            $this->folio = $data['folio'];
            $this->no_oficio = $data['no_oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->no_soporte = $data['no_soporte'];
            $this->fecha_soporte = $data['fecha_soporte'];
            $this->firmante_soporte = $data['firmante_soporte'];
            $this->cargo_firmante = $data['cargo_firmante'];

            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para seleccionar los datos  de la tabla admtbl_permisos para el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos 
     */
    public function selectListTramite($sqlWhere='', $sqlValues=array() )
    {
        $sql = "select id_permisos, fecha_ini,tipo_licencia, fecha_fin, num_dias, motivo,folio  
                from admtbl_permisos
                ";
        if (!empty($sqlWhere))
            $sql .= " WHERE  $sqlWhere";                
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            //echo $sql;
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $tipo_licencia = ($data['tipo_licencia'] == 1)? "CON SUELDO":"SIN SUELDO";
                $datos[] = array(
                               'id_permisos' => $data['id_permisos'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'tipo_licencia' => $tipo_licencia,
                               'num_dias' => $data['num_dias'],
                               'motivo' => $data['motivo'],
                               'folio' => $data['folio'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funcion para mostrar los options en un selec con datos estatitos
     * @param array $datos, arreglo que contiene los datos 
     * @param int $id, el id  seleccionado por default

    */
    public function shwCatEstatico($aryDatos, $id){
        //$aryDatos = $this->selectAll('modo='.$tipo, 'id_tipo_cambio Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["value"] )
                $html .= '<option value="'.$datos["value"].'" selected>'.$datos["descripcion"].'</option>';
            else
                $html .= '<option value="'.$datos["value"].'" >'.$datos["descripcion"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_permisos, a.curp, a.fecha_reg, a.fecha_ini, a.fecha_fin, a.num_dias, a.indefinida, a.tipo_licencia, a.prorroga, a.motivo, a.opinion, a.titular, a.tajeta, a.nombramiento, a.lugar_checa, a.comision, a.sueldo, a.ofi_paga, a.fecha_ini_ant, a.fecha_fin_ant, a.num_dias_ant, a.tipo_licencia_ant, a.prorroga_ant, a.folio, a.no_oficio, a.fecha_oficio, a.no_soporte, a.fecha_soporte, a.firmante_soporte, a.cargo_firmante,
                  b.curp, b.nombre, b.a_paterno, b.a_materno, b.fecha_nac, b.genero, b.rfc, b.cuip, b.folio_ife, b.mat_cartilla, b.licencia_conducir, b.pasaporte, b.id_estado_civil, b.id_tipo_sangre, b.id_nacionalidad, b.id_entidad, b.id_municipio, b.lugar_nac, b.id_status
                FROM admtbl_permisos a 
                 LEFT JOIN admtbl_datos_personales b ON a.curp=b.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_permisos' => $data['id_permisos'],
                               'curp' => $data['curp'],
                               'fecha_reg' => $data['fecha_reg'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'num_dias' => $data['num_dias'],
                               'indefinida' => $data['indefinida'],
                               'tipo_licencia' => $data['tipo_licencia'],
                               'prorroga' => $data['prorroga'],
                               'motivo' => $data['motivo'],
                               'opinion' => $data['opinion'],
                               'titular' => $data['titular'],
                               'tajeta' => $data['tajeta'],
                               'nombramiento' => $data['nombramiento'],
                               'lugar_checa' => $data['lugar_checa'],
                               'comision' => $data['comision'],
                               'sueldo' => $data['sueldo'],
                               'ofi_paga' => $data['ofi_paga'],
                               'fecha_ini_ant' => $data['fecha_ini_ant'],
                               'fecha_fin_ant' => $data['fecha_fin_ant'],
                               'num_dias_ant' => $data['num_dias_ant'],
                               'tipo_licencia_ant' => $data['tipo_licencia_ant'],
                               'prorroga_ant' => $data['prorroga_ant'],
                               'folio' => $data['folio'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'no_soporte' => $data['no_soporte'],
                               'fecha_soporte' => $data['fecha_soporte'],
                               'firmante_soporte' => $data['firmante_soporte'],
                               'cargo_firmante' => $data['cargo_firmante'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_permisos( curp, fecha_reg, fecha_ini, fecha_fin, num_dias, indefinida, tipo_licencia, prorroga, motivo, opinion, titular, tajeta, nombramiento, lugar_checa, comision, sueldo, ofi_paga, fecha_ini_ant, fecha_fin_ant, num_dias_ant, tipo_licencia_ant, prorroga_ant, folio, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante)
                VALUES(:curp, :fecha_reg, :fecha_ini, :fecha_fin, :num_dias, :indefinida, :tipo_licencia, :prorroga, :motivo, :opinion, :titular, :tajeta, :nombramiento, :lugar_checa, :comision, :sueldo, :ofi_paga, :fecha_ini_ant, :fecha_fin_ant, :num_dias_ant, :tipo_licencia_ant, :prorroga_ant, :folio, :no_oficio, :fecha_oficio, :no_soporte, :fecha_soporte, :firmante_soporte, :cargo_firmante);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array( ":curp" => $this->curp, ":fecha_reg" => date("Y-m-d"), ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":num_dias" => $this->num_dias, ":indefinida" => $this->indefinida, ":tipo_licencia" => $this->tipo_licencia, ":prorroga" => $this->prorroga, ":motivo" => $this->motivo, ":opinion" => $this->opinion, ":titular" => $this->titular, ":tajeta" => $this->tajeta, ":nombramiento" => $this->nombramiento, ":lugar_checa" => $this->lugar_checa, ":comision" => $this->comision, ":sueldo" => $this->sueldo, ":ofi_paga" => $this->ofi_paga, ":fecha_ini_ant" => $this->fecha_ini_ant, ":fecha_fin_ant" => $this->fecha_fin_ant, ":num_dias_ant" => $this->num_dias_ant, ":tipo_licencia_ant" => $this->tipo_licencia_ant, ":prorroga_ant" => $this->prorroga_ant, ":folio" => $this->folio, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_permisos
                   SET curp=:curp,  fecha_ini=:fecha_ini, fecha_fin=:fecha_fin, num_dias=:num_dias, indefinida=:indefinida, tipo_licencia=:tipo_licencia, prorroga=:prorroga, motivo=:motivo, opinion=:opinion, titular=:titular, tajeta=:tajeta, nombramiento=:nombramiento, lugar_checa=:lugar_checa, comision=:comision, sueldo=:sueldo, ofi_paga=:ofi_paga, fecha_ini_ant=:fecha_ini_ant, fecha_fin_ant=:fecha_fin_ant, num_dias_ant=:num_dias_ant, tipo_licencia_ant=:tipo_licencia_ant, prorroga_ant=:prorroga_ant, folio=:folio, no_oficio=:no_oficio, fecha_oficio=:fecha_oficio, no_soporte=:no_soporte, fecha_soporte=:fecha_soporte, firmante_soporte=:firmante_soporte, cargo_firmante=:cargo_firmante
                WHERE id_permisos=:id_permisos;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_permisos" => $this->id_permisos, ":curp" => $this->curp,  ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":num_dias" => $this->num_dias, ":indefinida" => $this->indefinida, ":tipo_licencia" => $this->tipo_licencia, ":prorroga" => $this->prorroga, ":motivo" => $this->motivo, ":opinion" => $this->opinion, ":titular" => $this->titular, ":tajeta" => $this->tajeta, ":nombramiento" => $this->nombramiento, ":lugar_checa" => $this->lugar_checa, ":comision" => $this->comision, ":sueldo" => $this->sueldo, ":ofi_paga" => $this->ofi_paga, ":fecha_ini_ant" => $this->fecha_ini_ant, ":fecha_fin_ant" => $this->fecha_fin_ant, ":num_dias_ant" => $this->num_dias_ant, ":tipo_licencia_ant" => $this->tipo_licencia_ant, ":prorroga_ant" => $this->prorroga_ant, ":folio" => $this->folio, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {
        $sql = "DELETE FROM admtbl_permisos
                WHERE id_permisos=:id_permisos 
                AND curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_permisos" => $this->id_permisos, ":curp" => $this->curp));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    
}


?>