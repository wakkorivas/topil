<?php
/**
 * @author SisA
 * @copyright 2014
 *
 * La clase Resolve: Para la gesti�n del inicio de sesi�n de los usuarios...
 *
 */
class Session
{
    private $xResult = 0;
      
    public function __construct($prm1, $prm2){
        require_once('mysql.class.php');
        $objBd = New MySQLPDO(); 
        
        $qSql = $objBd->prepare("SELECT id_usuario 
                                 FROM xtblusuarios 
                                 WHERE nom_usr=:prm1 AND pswd=sha1(:prm2);");
        $aryParam = array(':prm1' => $prm1, ':prm2' => $prm2);
        if ($qSql->execute($aryParam)) {
            $this->xResult = $qSql->fetchColumn();
        } 
    }
      
    public function getResult(){
        return $this->xResult;
    }
}

?>