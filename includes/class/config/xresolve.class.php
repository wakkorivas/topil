<?php
class Resolve{
    private $xResult = 0;
      
    public function Resolve($prm1, $prm2){
        require_once("mysql.class.php");
        $xbd = New MySQLPDO(); 
        
        $qSql = $xbd->dbh->prepare("SELECT id_usuario 
                                    FROM xtbusuario 
                                    WHERE nom_usr=:prm1 AND pswd=sha1(:prm2);");
        $aryParam = array(":prm1" => $prm1, ":prm2" => $prm2);
        if( $qSql->execute($aryParam) ){
            $this->xResult = $qSql->fetchColumn();
        } 
    }
      
    public function getResult(){
        return $this->xResult;
    }
}
?>