<?php
/**
 *
 */
class LogcatArmFolioc
{
    public $id_folioc; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $folioc; /** @Tipo: varchar(8), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de folios dentro de un combobox.
     * @param int $id, id del folioc seleccionado por deafult     
     * @return array html(options)
     */
    public function shwFolioc( $id=0 ){
        $aryDatos = $this->selectAll('a.xstat=1', 'a.folioc Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_folioc"] )
                $html .= '<option value="'.$datos["id_folioc"].'" selected>'.$datos["folioc"].'</option>';
            else
                $html .= '<option value="'.$datos["id_folioc"].'" >'.$datos["folioc"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_folioc)
    {
        $sql = "SELECT id_folioc, folioc
                FROM logcat_arm_folioc
                WHERE id_folioc=:id_folioc;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_folioc' => $id_folioc));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_folioc = $data['id_folioc'];
            $this->folioc = $data['folioc'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_folioc, a.folioc, a.xstat
                FROM logcat_arm_folioc a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_folioc' => $data['id_folioc'],
                               'folioc' => $data['folioc'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_folioc(id_folioc, folioc, xstat)
                VALUES(:id_folioc, :folioc, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_folioc" => $this->id_folioc, ":folioc" => $this->folioc, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_folioc
                   SET folioc=:folioc, xstat=:xstat
                WHERE id_folioc=:id_folioc;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_folioc" => $this->id_folioc, ":folioc" => $this->folioc, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>