<?php
/**
 *
 */
class LogcatArmPropietario
{
    public $id_propietario; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $propietario; /** @Tipo: varchar(15), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de propietarios dentro de un combobox.
     * @param int $id, id del propietario seleccionado por deafult     
     * @return array html(options)
     */
    public function shwPropietario( $id=0 ){
        $aryDatos = $this->selectAll('a.xstat=1', 'a.propietario Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_propietario"] )
                $html .= '<option value="'.$datos["id_propietario"].'" selected>'.$datos["propietario"].'</option>';
            else
                $html .= '<option value="'.$datos["id_propietario"].'" >'.$datos["propietario"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_propietario)
    {
        $sql = "SELECT id_propietario, propietario, xstat
                FROM logcat_arm_propietario
                WHERE id_propietario=:id_propietario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_propietario' => $id_propietario));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_propietario = $data['id_propietario'];
            $this->propietario = $data['propietario'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_propietario, a.propietario, a.xstat
                FROM logcat_arm_propietario a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_propietario' => $data['id_propietario'],
                               'propietario' => $data['propietario'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_propietario(id_propietario, propietario, xstat)
                VALUES(:id_propietario, :propietario, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_propietario" => $this->id_propietario, ":propietario" => $this->propietario, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_propietario
                   SET propietario=:propietario, xstat=:xstat
                WHERE id_propietario=:id_propietario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_propietario" => $this->id_propietario, ":propietario" => $this->propietario, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>