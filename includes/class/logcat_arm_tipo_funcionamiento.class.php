<?php
/**
 *
 */
class LogcatArmTipoFuncionamiento
{
    public $id_tipo_funcionamiento; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_funcionamiento; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de tipo de funcionamiento dentro de un combobox.
     * @param int $id, id del tipo de funcionamiento seleccionada por deafult     
     * @return array html(options)
     */
    public function shwTipoFun($id=0){
        $aryDatos = $this->selectAll('a.xstat=1', 'a.tipo_funcionamiento Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_funcionamiento"] )
                $html .= '<option value="'.$datos["id_tipo_funcionamiento"].'" selected>'.$datos["tipo_funcionamiento"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_funcionamiento"].'" >'.$datos["tipo_funcionamiento"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_funcionamiento)
    {
        $sql = "SELECT id_tipo_funcionamiento, tipo_funcionamiento, xstat
                FROM logcat_arm_tipo_funcionamiento
                WHERE id_tipo_funcionamiento=:id_tipo_funcionamiento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_funcionamiento' => $id_tipo_funcionamiento));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_funcionamiento = $data['id_tipo_funcionamiento'];
            $this->tipo_funcionamiento = $data['tipo_funcionamiento'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_tipo_funcionamiento, a.tipo_funcionamiento, a.xstat
                FROM logcat_arm_tipo_funcionamiento a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_funcionamiento' => $data['id_tipo_funcionamiento'],
                               'tipo_funcionamiento' => $data['tipo_funcionamiento'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_tipo_funcionamiento(id_tipo_funcionamiento, tipo_funcionamiento, xstat)
                VALUES(:id_tipo_funcionamiento, :tipo_funcionamiento, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_funcionamiento" => $this->id_tipo_funcionamiento, ":tipo_funcionamiento" => $this->tipo_funcionamiento, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_tipo_funcionamiento
                   SET tipo_funcionamiento=:tipo_funcionamiento, xstat=:xstat
                WHERE id_tipo_funcionamiento=:id_tipo_funcionamiento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_funcionamiento" => $this->id_tipo_funcionamiento, ":tipo_funcionamiento" => $this->tipo_funcionamiento, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>