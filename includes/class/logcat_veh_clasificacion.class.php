<?php
/**
 *
 */
class LogcatVehClasificacion
{
    public $id_clasificacion; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $clasificacion; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
	
	 /**
     * Funci�n para mostrar la lista de clasificaciones dentro de un combobox.
     * @param int $id, id de la clasificacion seleccionado por deafult     
     * @return array html(options)
     */
    public function shwClasificacion($id=0){
        $aryDatos = $this->selectAll('a.xstat=1 ', 'clasificacion Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_clasificacion"] )
                $html .= '<option value="'.$datos["id_clasificacion"].'" selected>'.$datos["clasificacion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_clasificacion"].'" >'.$datos["clasificacion"].'</option>';
        }
        return $html;
    }

	public function getClasificacion( $id_tipo ){
	
		$sql = "SELECT c.id_clasificacion
                FROM logcat_veh_clasificacion as c
				INNER JOIN logcat_veh_tipo as t On t.id_clasificacion=c.id_clasificacion
				WHERE t.id_tipo=:id_tipo
				AND t.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo' => $id_tipo, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            return $data['id_clasificacion'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_clasificacion)
    {
        $sql = "SELECT id_clasificacion, clasificacion, xstat
                FROM logcat_veh_clasificacion
                WHERE id_clasificacion=:id_clasificacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_clasificacion' => $id_clasificacion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_clasificacion = $data['id_clasificacion'];
            $this->clasificacion = $data['clasificacion'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_clasificacion, a.clasificacion, a.xstat
                FROM logcat_veh_clasificacion a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_clasificacion' => $data['id_clasificacion'],
                               'clasificacion' => $data['clasificacion'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_clasificacion(id_clasificacion, clasificacion, xstat)
                VALUES(:id_clasificacion, :clasificacion, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_clasificacion" => $this->id_clasificacion, ":clasificacion" => $this->clasificacion, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_clasificacion
                   SET clasificacion=:clasificacion, xstat=:xstat
                WHERE id_clasificacion=:id_clasificacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_clasificacion" => $this->id_clasificacion, ":clasificacion" => $this->clasificacion, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>