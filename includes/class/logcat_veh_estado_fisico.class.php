<?php
/**
 *
 */
class LogcatVehEstadoFisico
{
    public $id_estado_fisico; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $estado_fisico; /** @Tipo: varchar(20), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_estado_vehiculo_sn; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

	/**
     * Funci�n para mostrar la lista de estados fisico dentro de un combobox.
     * @param int $id, id del estado fisico seleccionado por deafult     
     * @return array html(options)
     */
    public function shwEstadoFisico($id=0){
        $aryDatos = $this->selectAll('xstat=1', 'estado_fisico Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_estado_fisico"] )
                $html .= '<option value="'.$datos["id_estado_fisico"].'" selected>'.$datos["estado_fisico"].'</option>';
            else
                $html .= '<option value="'.$datos["id_estado_fisico"].'" >'.$datos["estado_fisico"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_estado_fisico)
    {
        $sql = "SELECT id_estado_fisico, estado_fisico, id_estado_vehiculo_sn, xstat
                FROM logcat_veh_estado_fisico
                WHERE id_estado_fisico=:id_estado_fisico;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_estado_fisico' => $id_estado_fisico));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_estado_fisico = $data['id_estado_fisico'];
            $this->estado_fisico = $data['estado_fisico'];
            $this->id_estado_vehiculo_sn = $data['id_estado_vehiculo_sn'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_estado_fisico, a.estado_fisico, a.id_estado_vehiculo_sn, a.xstat
                FROM logcat_veh_estado_fisico a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_estado_fisico' => $data['id_estado_fisico'],
                               'estado_fisico' => $data['estado_fisico'],
                               'id_estado_vehiculo_sn' => $data['id_estado_vehiculo_sn'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_estado_fisico(id_estado_fisico, estado_fisico, id_estado_vehiculo_sn, xstat)
                VALUES(:id_estado_fisico, :estado_fisico, :id_estado_vehiculo_sn, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estado_fisico" => $this->id_estado_fisico, ":estado_fisico" => $this->estado_fisico, ":id_estado_vehiculo_sn" => $this->id_estado_vehiculo_sn, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_estado_fisico
                   SET estado_fisico=:estado_fisico, id_estado_vehiculo_sn=:id_estado_vehiculo_sn, xstat=:xstat
                WHERE id_estado_fisico=:id_estado_fisico;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_estado_fisico" => $this->id_estado_fisico, ":estado_fisico" => $this->estado_fisico, ":id_estado_vehiculo_sn" => $this->id_estado_vehiculo_sn, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>