<?php
/**
 *
 */
class LogcatVehTipoServicio
{
    public $id_tipo_servicio; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_servicio; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

	/**
     * Funci�n para mostrar la lista de tipo de servicios dentro de un combobox.
     * @param int $id, id del tipo de servicio seleccionado por deafult     
     * @return array html(options)
     */
    public function shwTipoServicio($id=0){
        $aryDatos = $this->selectAll('xstat=1', 'tipo_servicio Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_servicio"] )
                $html .= '<option value="'.$datos["id_tipo_servicio"].'" selected>'.$datos["tipo_servicio"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_servicio"].'" >'.$datos["tipo_servicio"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_servicio)
    {
        $sql = "SELECT id_tipo_servicio, tipo_servicio, xstat
                FROM logcat_veh_tipo_servicio
                WHERE id_tipo_servicio=:id_tipo_servicio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_servicio' => $id_tipo_servicio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_servicio = $data['id_tipo_servicio'];
            $this->tipo_servicio = $data['tipo_servicio'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_tipo_servicio, a.tipo_servicio, a.xstat
                FROM logcat_veh_tipo_servicio a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_servicio' => $data['id_tipo_servicio'],
                               'tipo_servicio' => $data['tipo_servicio'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_tipo_servicio(id_tipo_servicio, tipo_servicio, xstat)
                VALUES(:id_tipo_servicio, :tipo_servicio, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_servicio" => $this->id_tipo_servicio, ":tipo_servicio" => $this->tipo_servicio, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_tipo_servicio
                   SET tipo_servicio=:tipo_servicio, xstat=:xstat
                WHERE id_tipo_servicio=:id_tipo_servicio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_servicio" => $this->id_tipo_servicio, ":tipo_servicio" => $this->tipo_servicio, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>