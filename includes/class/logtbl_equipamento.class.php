<?php
/**
 *
 */
class LogtblEquipamento
{
    public $id_equipamento; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_marca; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_dependencia; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $inventario; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $serie; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $modelo; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_estatus; /** @Tipo: tinyint(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatEqMarca; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatEqTipo; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatEntidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatEqDependencia; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatEqEstatus; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_eq_marca.class.php';
        require_once 'logcat_eq_tipo.class.php';
        require_once 'admcat_entidades.class.php';
        require_once 'logcat_eq_dependencia.class.php';
        require_once 'logcat_eq_estatus.class.php';
        $this->LogcatEqMarca = new LogcatEqMarca();
        $this->LogcatEqTipo = new LogcatEqTipo();
        $this->AdmcatEntidades = new AdmcatEntidades();
        $this->LogcatEqDependencia = new LogcatEqDependencia();
        $this->LogcatEqEstatus = new LogcatEqEstatus();
    }

    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }
    
    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.id_equipamento, mar.marca, t.tipo, e.entidad, d.dependencia, a.inventario, a.serie, a.modelo, es.estatus
                FROM logtbl_equipamento a                     
                    LEFT JOIN logcat_eq_marca as mar On mar.id_marca = a.id_marca
                    LEFT JOIN logcat_eq_tipo as t On t.id_tipo = a.id_tipo
                    LEFT JOIN admcat_entidades as e On e.id_entidad = a.id_entidad
                    LEFT JOIN logcat_eq_dependencia as d On d.id_dependencia = a.id_dependencia 
                    LEFT JOIN logcat_eq_estatus as es On es.id_estatus = a.id_estatus ";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_equipamento' => $data['id_equipamento'],
                               'marca' => $data['marca'],
                               'tipo' => $data['tipo'],
                               'inventario' => $data['inventario'],
                               'serie' => $data['serie'],
                               'modelo' => $data['modelo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }        
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {    
        $sql = "SELECT COUNT(*) 
                FROM logtbl_equipamento as a               
                    LEFT JOIN logcat_eq_marca as mar On mar.id_marca = a.id_marca
                    LEFT JOIN logcat_eq_tipo as t On t.id_tipo = a.id_tipo
                    LEFT JOIN admcat_entidades as e On e.id_entidad = a.id_entidad
                    LEFT JOIN logcat_eq_dependencia as d On d.id_dependencia = a.id_dependencia 
                    LEFT JOIN logcat_eq_estatus as es On es.id_estatus = a.id_estatus";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }         

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_equipamento)
    {
        $sql = "SELECT id_equipamento, id_marca, id_tipo, id_entidad, id_dependencia, inventario, serie, modelo, id_estatus
                FROM logtbl_equipamento
                WHERE id_equipamento=:id_equipamento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_equipamento' => $id_equipamento));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_equipamento = $data['id_equipamento'];
            $this->id_marca = $data['id_marca'];
            $this->id_tipo = $data['id_tipo'];
            $this->id_entidad = $data['id_entidad'];
            $this->id_dependencia = $data['id_dependencia'];
            $this->inventario = $data['inventario'];
            $this->serie = $data['serie'];
            $this->modelo = $data['modelo'];
            $this->id_estatus = $data['id_estatus'];

            $this->LogcatEqMarca->select($this->id_marca);
            $this->LogcatEqTipo->select($this->id_tipo);
            $this->AdmcatEntidades->select($this->id_entidad);
            $this->LogcatEqDependencia->select($this->id_dependencia);
            $this->LogcatEqEstatus->select($this->id_estatus);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_equipamento, a.id_marca, a.id_tipo, a.id_entidad, a.id_dependencia, a.inventario, a.serie, a.modelo, a.id_estatus,
                  b.id_marca, b.marca, b.xstat,
                  c.id_tipo, c.tipo, c.xstat,
                  d.id_entidad, d.entidad, d.abreviacion, d.id_pais,
                  e.id_dependencia, e.dependencia, e.xstat,
                  f.id_estatus, f.estatus, f.xstat
                FROM logtbl_equipamento a 
                 LEFT JOIN logcat_eq_marca b ON a.id_marca=b.id_marca
                 LEFT JOIN logcat_eq_tipo c ON a.id_tipo=c.id_tipo
                 LEFT JOIN admcat_entidades d ON a.id_entidad=d.id_entidad
                 LEFT JOIN logcat_eq_dependencia e ON a.id_dependencia=e.id_dependencia
                 LEFT JOIN logcat_eq_estatus f ON a.id_estatus=f.id_estatus";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_equipamento' => $data['id_equipamento'],
                               'id_marca' => $data['id_marca'],
                               'id_tipo' => $data['id_tipo'],
                               'id_entidad' => $data['id_entidad'],
                               'id_dependencia' => $data['id_dependencia'],
                               'inventario' => $data['inventario'],
                               'serie' => $data['serie'],
                               'modelo' => $data['modelo'],
                               'id_estatus' => $data['id_estatus'],
                               'logcat_eq_marca_marca' => $data['marca'],
                               'logcat_eq_marca_xstat' => $data['xstat'],
                               'logcat_eq_tipo_tipo' => $data['tipo'],
                               'logcat_eq_tipo_xstat' => $data['xstat'],
                               'admcat_entidades_entidad' => $data['entidad'],
                               'admcat_entidades_abreviacion' => $data['abreviacion'],
                               'admcat_entidades_id_pais' => $data['id_pais'],
                               'logcat_eq_dependencia_dependencia' => $data['dependencia'],
                               'logcat_eq_dependencia_xstat' => $data['xstat'],
                               'logcat_eq_estatus_estatus' => $data['estatus'],
                               'logcat_eq_estatus_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_equipamento(id_marca, id_tipo, id_entidad, id_dependencia, inventario, serie, modelo, id_estatus)
                VALUES(:id_marca, :id_tipo, :id_entidad, :id_dependencia, :inventario, :serie, :modelo, :id_estatus);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_marca" => $this->id_marca, ":id_tipo" => $this->id_tipo, ":id_entidad" => $this->id_entidad, ":id_dependencia" => $this->id_dependencia, ":inventario" => $this->inventario, ":serie" => $this->serie, ":modelo" => $this->modelo, ":id_estatus" => $this->id_estatus));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_equipamento
                   SET id_marca=:id_marca, id_tipo=:id_tipo, id_entidad=:id_entidad, id_dependencia=:id_dependencia, inventario=:inventario, serie=:serie, modelo=:modelo
                WHERE id_equipamento=:id_equipamento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_equipamento" => $this->id_equipamento, ":id_marca" => $this->id_marca, ":id_tipo" => $this->id_tipo, ":id_entidad" => $this->id_entidad, ":id_dependencia" => $this->id_dependencia, ":inventario" => $this->inventario, ":serie" => $this->serie, ":modelo" => $this->modelo));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>