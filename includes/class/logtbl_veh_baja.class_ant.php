<?php
/**
 *
 */
class LogtblVehBaja
{
    public $id_vehiculo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $num_lote; /** @Tipo: varchar(15), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_motivo_baja; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_baja; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblVehiculos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehMotivoBaja; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_vehiculos.class.php';
        require_once 'logcat_veh_motivo_baja.class.php';
        $this->LogtblVehiculos = new LogtblVehiculos();
        $this->LogcatVehMotivoBaja = new LogcatVehMotivoBaja();
    }	

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_vehiculo, $id_motivo_baja)
    {
        $sql = "SELECT id_vehiculo, num_lote, id_motivo_baja, fecha_baja
                FROM logtbl_veh_baja
                WHERE id_vehiculo=:id_vehiculo AND id_motivo_baja=:id_motivo_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_vehiculo' => $id_vehiculo, ':id_motivo_baja' => $id_motivo_baja));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->num_lote = $data['num_lote'];
            $this->id_motivo_baja = $data['id_motivo_baja'];
            $this->fecha_baja = $data['fecha_baja'];

            $this->LogtblVehiculos->select($this->id_vehiculo);
            $this->LogcatVehMotivoBaja->select($this->id_motivo_baja);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_vehiculo, a.num_lote, a.id_motivo_baja, a.fecha_baja,
                  b.id_vehiculo, b.num_serie, b.num_economico, b.num_motor, b.poliza, b.inciso, b.placas, b.num_placas, b.num_cilindros, b.foto_frente, b.foto_lat_der, b.foto_lat_izq, b.foto_posterior, b.foto_interior, b.id_estado_fisico, b.id_situacion, b.id_modelo, b.id_transmision, b.id_color, b.id_tipo,
                  c.id_motivo_baja, c.motivo_baja, c.xstat
                FROM logtbl_veh_baja a 
                 LEFT JOIN logtbl_vehiculos b ON a.id_vehiculo=b.id_vehiculo
                 LEFT JOIN logcat_veh_motivo_baja c ON a.id_motivo_baja=c.id_motivo_baja";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'num_lote' => $data['num_lote'],
                               'id_motivo_baja' => $data['id_motivo_baja'],
                               'fecha_baja' => $data['fecha_baja'],
                               'logtbl_vehiculos_num_serie' => $data['num_serie'],
                               'logtbl_vehiculos_num_economico' => $data['num_economico'],
                               'logtbl_vehiculos_num_motor' => $data['num_motor'],
                               'logtbl_vehiculos_poliza' => $data['poliza'],
                               'logtbl_vehiculos_inciso' => $data['inciso'],
                               'logtbl_vehiculos_placas' => $data['placas'],
                               'logtbl_vehiculos_num_placas' => $data['num_placas'],
                               'logtbl_vehiculos_num_cilindros' => $data['num_cilindros'],
                               'logtbl_vehiculos_foto_frente' => $data['foto_frente'],
                               'logtbl_vehiculos_foto_lat_der' => $data['foto_lat_der'],
                               'logtbl_vehiculos_foto_lat_izq' => $data['foto_lat_izq'],
                               'logtbl_vehiculos_foto_posterior' => $data['foto_posterior'],
                               'logtbl_vehiculos_foto_interior' => $data['foto_interior'],
                               'logtbl_vehiculos_id_estado_fisico' => $data['id_estado_fisico'],
                               'logtbl_vehiculos_id_situacion' => $data['id_situacion'],
                               'logtbl_vehiculos_id_modelo' => $data['id_modelo'],
                               'logtbl_vehiculos_id_transmision' => $data['id_transmision'],
                               'logtbl_vehiculos_id_color' => $data['id_color'],
                               'logtbl_vehiculos_id_tipo' => $data['id_tipo'],
                               'logcat_veh_motivo_baja_motivo_baja' => $data['motivo_baja'],
                               'logcat_veh_motivo_baja_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlValues, cadena que contiene el parametro de busqueda.
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos     
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
     /*
     create view viewvehlstbajas as
SELECT a.id_vehiculo, a.num_serie, a.num_economico, a.num_motor, a.poliza, a.inciso, a.placas, a.num_placas,
                a.tarjeta_circulacion, a.reg_fed_veh, a.num_puertas, a.num_cilindros, a.foto_frente,
                a.foto_lat_der, a.foto_lat_izq, a.foto_posterior, a.foto_interior,
                  b.color, b.color_hex,
                  c.estado_fisico, c.id_estado_vehiculo_sn,
                  d.modelo,
                  e.situacion,
                  f.tipo, f.id_tipo_sn, f.xstat, f.id_marca,
                  j.clasificacion,
                  k.marca,
                  es.estado,
                  g.transmision,
                  h.motivo_baja,
                  i.num_lote, i.fecha_baja

                 FROM logtbl_vehiculos a
                 LEFT JOIN logcat_veh_color b ON a.id_color=b.id_color
                 LEFT JOIN logcat_veh_estado_fisico c ON a.id_estado_fisico=c.id_estado_fisico
                 LEFT JOIN logcat_veh_modelo d ON a.id_modelo=d.id_modelo
                 LEFT JOIN logcat_veh_situacion e ON a.id_situacion=e.id_situacion
                 LEFT JOIN logcat_veh_tipo f ON a.id_tipo=f.id_tipo
                 LEFT JOIN logcat_veh_clasificacion j ON f.id_clasificacion=j.id_clasificacion
                 LEFT JOIN logcat_veh_marca k ON f.id_marca=k.id_marca
                 LEFT JOIN logcat_veh_estado as es ON es.id_estado=a.id_estado
                 LEFT JOIN logcat_veh_transmision g ON a.id_transmision=g.id_transmision
                 LEFT JOIN logtbl_veh_baja i ON a.id_vehiculo=i.id_vehiculo
                 LEFT JOIN logcat_veh_motivo_baja h ON i.id_motivo_baja=h.id_motivo_baja
where es.id_estado = 2
     */
    public function selectLstBajas($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "select inventario, serie, marca, modelo, tipo, fecha_baja from viewvehlstbajas ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";        
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_equipamento' => $data['id_equipamento'],
                               'inventario' => $data['inventario'],
                               'serie' => $data['serie'],
                               'marca' => $data['marca'],
                               'modelo' => $data['modelo'],
                               'tipo' => $data['tipo'],
                               'fecha_baja' => $data['fecha_baja'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_veh_baja(id_vehiculo, num_lote, id_motivo_baja, fecha_baja)
                VALUES(:id_vehiculo, :num_lote, :id_motivo_baja, :fecha_baja);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":num_lote" => $this->num_lote, ":id_motivo_baja" => $this->id_motivo_baja, ":fecha_baja" => $this->fecha_baja));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_veh_baja
                   SET num_lote=:num_lote, fecha_baja=:fecha_baja
                WHERE id_vehiculo=:id_vehiculo AND id_motivo_baja=:id_motivo_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":num_lote" => $this->num_lote, ":id_motivo_baja" => $this->id_motivo_baja, ":fecha_baja" => $this->fecha_baja));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para actualizar datos de la tabla de logtbl_vehiculos en el proceso de baja
     * @return boolean true si el proceso es satisfactorio
     */
    public function update_baja()
    {
        $sql = "UPDATE logtbl_vehiculos
                   SET id_estado=:id_estado 
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":id_estado" => 2 ));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>