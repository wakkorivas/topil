<?php
/**
 *
 */
class OpecatMidArmasRoles
{
    public $id_arma_rol; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $arma_rol; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_Armas_Roles($id=0){
        $aryDatos = $this->selectAll('id_arma_rol Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_arma_rol"] )
                $html .= '<option value="'.$datos["id_arma_rol"].'" selected>'.$datos["arma_rol"].'</option>';
            else
                $html .= '<option value="'.$datos["id_arma_rol"].'" >'.$datos["arma_rol"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_arma_rol)
    {
        $sql = "SELECT id_arma_rol, arma_rol
                FROM opecat_mid_armas_roles
                WHERE id_arma_rol=:id_arma_rol;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_arma_rol' => $id_arma_rol));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_arma_rol = $data['id_arma_rol'];
            $this->arma_rol = $data['arma_rol'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sql_order="", $sql_limit="")
    {
        $sql = "SELECT a.id_arma_rol, a.arma_rol
                FROM opecat_mid_armas_roles a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_arma_rol' => $data['id_arma_rol'],
                               'arma_rol' => $data['arma_rol'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_armas_roles(id_arma_rol, arma_rol)
                VALUES(:id_arma_rol, :arma_rol);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_arma_rol" => $this->id_arma_rol, ":arma_rol" => $this->arma_rol));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_armas_roles
                   SET arma_rol=:arma_rol
                WHERE id_arma_rol=:id_arma_rol;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_arma_rol" => $this->id_arma_rol, ":arma_rol" => $this->arma_rol));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>