<?php
/**
 *
 */
class OpecatMidCuartelesTipos
{
    public $id_cuartel_tipo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $cuartel_tipo; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Tipos_Cuarteles($id=0){
        $aryDatos = $this->selectAll('id_cuartel_tipo Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_cuartel_tipo"] )
                $html .= '<option value="'.$datos["id_cuartel_tipo"].'" selected>'.$datos["cuartel_tipo"].'</option>';
            else
                $html .= '<option value="'.$datos["id_cuartel_tipo"].'" >'.$datos["cuartel_tipo"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_cuartel_tipo)
    {
        $sql = "SELECT id_cuartel_tipo, cuartel_tipo
                FROM opecat_mid_cuarteles_tipos
                WHERE id_cuartel_tipo=:id_cuartel_tipo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_cuartel_tipo' => $id_cuartel_tipo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_cuartel_tipo = $data['id_cuartel_tipo'];
            $this->cuartel_tipo = $data['cuartel_tipo'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_cuartel_tipo, a.cuartel_tipo
                FROM opecat_mid_cuarteles_tipos a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_cuartel_tipo' => $data['id_cuartel_tipo'],
                               'cuartel_tipo' => $data['cuartel_tipo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_cuarteles_tipos(id_cuartel_tipo, cuartel_tipo)
                VALUES(:id_cuartel_tipo, :cuartel_tipo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cuartel_tipo" => $this->id_cuartel_tipo, ":cuartel_tipo" => $this->cuartel_tipo));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_cuarteles_tipos
                   SET cuartel_tipo=:cuartel_tipo
                WHERE id_cuartel_tipo=:id_cuartel_tipo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_cuartel_tipo" => $this->id_cuartel_tipo, ":cuartel_tipo" => $this->cuartel_tipo));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>