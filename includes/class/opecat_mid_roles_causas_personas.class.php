<?php
/**
 *
 */
class OpecatMidRolesCausasPersonas
{
    public $id_rol_persona; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_causa_persona; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidRolesPersonas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidCausasPersonas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_roles_personas.class.php';
        require_once 'opecat_mid_causas_personas.class.php';
        $this->OpecatMidRolesPersonas = new OpecatMidRolesPersonas();
        $this->OpecatMidCausasPersonas = new OpecatMidCausasPersonas();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_rol_persona, $id_causa_persona)
    {
        $sql = "SELECT id_rol_persona, id_causa_persona
                FROM opecat_mid_roles_causas_personas
                WHERE id_rol_persona=:id_rol_persona AND id_causa_persona=:id_causa_persona;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_rol_persona' => $id_rol_persona, ':id_causa_persona' => $id_causa_persona));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_rol_persona = $data['id_rol_persona'];
            $this->id_causa_persona = $data['id_causa_persona'];

            $this->OpecatMidRolesPersonas->select($this->id_rol_persona);
            $this->OpecatMidCausasPersonas->select($this->id_causa_persona);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_rol_persona, a.id_causa_persona,
                  b.id_rol_persona, b.rol_persona,
                  c.id_causa_persona, c.causa_persona
                FROM opecat_mid_roles_causas_personas a 
                 LEFT JOIN opecat_mid_roles_personas b ON a.id_rol_persona=b.id_rol_persona
                 LEFT JOIN opecat_mid_causas_personas c ON a.id_causa_persona=c.id_causa_persona";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_rol_persona' => $data['id_rol_persona'],
                               'id_causa_persona' => $data['id_causa_persona'],
                               'opecat_mid_roles_personas_rol_persona' => $data['rol_persona'],
                               'opecat_mid_causas_personas_causa_persona' => $data['causa_persona'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_roles_causas_personas(id_rol_persona, id_causa_persona)
                VALUES(:id_rol_persona, :id_causa_persona);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_rol_persona" => $this->id_rol_persona, ":id_causa_persona" => $this->id_causa_persona));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_roles_causas_personas
                   SET 
                WHERE id_rol_persona=:id_rol_persona AND id_causa_persona=:id_causa_persona;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_rol_persona" => $this->id_rol_persona, ":id_causa_persona" => $this->id_causa_persona));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>