<?php
/**
 *
 */
class OpecatMidTiposGeneralidades
{
    public $id_tipo_generalidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_generalidad; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

     /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Tipos_Generalidades($id_tipo_generalidad=0){
        $aryDatos = $this->selectAll('id_tipo_generalidad Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id_tipo_generalidad == $datos["id_tipo_generalidad"] )
                $html .= '<option value="'.$datos["id_tipo_generalidad"].'" selected>'.$datos["tipo_generalidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_generalidad"].'" >'.$datos["tipo_generalidad"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_generalidad_tipo)
    {
        $sql = "SELECT id_tipo_generalidad, tipo_generalidad
                FROM opecat_mid_tipos_generalidades
                WHERE id_tipo_generalidad=:id_tipo_generalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_generalidad' => $id_tipo_generalidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_generalidad_tipo = $data['id_tipo_generalidad'];
            $this->generalidad_tipo = $data['tipo_generalidad'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_tipo_generalidad, a.tipo_generalidad
                FROM opecat_mid_tipos_generalidades a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_generalidad' => $data['id_tipo_generalidad'],
                               'tipo_generalidad' => $data['tipo_generalidad'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_tipos_generalidades(id_tipo_generalidad, tipo_generalidad)
                VALUES(:id_tipo_generalidad, :tipo_generalidad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_generalidad" => $this->id_tipo_generalidad, ":tipo_generalidad" => $this->tipo_generalidad));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_tipos_generalidades
                   SET tipo_generalidad=:tipo_generalidad
                WHERE id_tipo_generalidad=:id_tipo_generalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_generalidad" => $this->id_tipo_generalidad, ":tipo_generalidad" => $this->tipo_generalidad));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>