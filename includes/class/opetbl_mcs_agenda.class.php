<?php
/**
 *
 */
class OpetblMcsAgenda
{
    public $id_agenda; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_municipio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_medio_convocatoria; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $hora_inicio; /** @Tipo: time, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $lugar; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_organizacion_gpo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $actividad_realizar; /** @Tipo: varchar(200), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $itinerarios_previso; /** @Tipo: varchar(200), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $motivo_evento; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $quien_informa; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_usuario; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $resultados; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMcsMediosConvocatorias; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMcsMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatOrganizacionesGrupos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mcs_medios_convocatorias.class.php';
        require_once 'opecat_mcs_municipios.class.php';
        require_once 'opecat_organizaciones_grupos.class.php';
        $this->OpecatMcsMediosConvocatorias = new OpecatMcsMediosConvocatorias();
        $this->OpecatMcsMunicipios = new OpecatMcsMunicipios();
        $this->OpecatOrganizacionesGrupos = new OpecatOrganizacionesGrupos();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_agenda)
    {
        $sql = "SELECT id_agenda, id_municipio, id_medio_convocatoria, fecha, hora_inicio, lugar, id_organizacion_gpo, actividad_realizar, itinerarios_previso, motivo_evento, quien_informa, id_usuario, resultados
                FROM opetbl_mcs_agenda
                WHERE id_agenda=:id_agenda;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_agenda' => $id_agenda));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_agenda = $data['id_agenda'];
            $this->id_municipio = $data['id_municipio'];
            $this->id_medio_convocatoria = $data['id_medio_convocatoria'];
            $this->fecha = $data['fecha'];
            $this->hora_inicio = $data['hora_inicio'];
            $this->lugar = $data['lugar'];
            $this->id_organizacion_gpo = $data['id_organizacion_gpo'];
            $this->actividad_realizar = $data['actividad_realizar'];
            $this->itinerarios_previso = $data['itinerarios_previso'];
            $this->motivo_evento = $data['motivo_evento'];
            $this->quien_informa = $data['quien_informa'];
            $this->id_usuario = $data['id_usuario'];
            $this->resultados = $data['resultados'];

            $this->OpecatMcsMediosConvocatorias->select($this->id_medio_convocatoria);
            $this->OpecatMcsMunicipios->select($this->id_municipio);
            $this->OpecatOrganizacionesGrupos->select($this->id_organizacion_gpo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_agenda, a.id_municipio, a.id_medio_convocatoria, a.fecha, a.hora_inicio, a.lugar, a.id_organizacion_gpo, a.actividad_realizar, a.itinerarios_previso, a.motivo_evento, a.quien_informa, a.id_usuario, a.resultados,
                  b.id_medio_convocatoria, b.medio_convocatoria,
                  c.id_municipio, c.id_estado, c.Municipio,
                  d.id_organizacion_gpo, d.organizacion_grupo, d.fecha_creacion, d.observaciones
                FROM opetbl_mcs_agenda a 
                 LEFT JOIN opecat_mcs_medios_convocatorias b ON a.id_medio_convocatoria=b.id_medio_convocatoria
                 LEFT JOIN opecat_mcs_municipios c ON a.id_municipio=c.id_municipio
                 LEFT JOIN opecat_organizaciones_grupos d ON a.id_organizacion_gpo=d.id_organizacion_gpo";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_agenda' => $data['id_agenda'],
                               'id_municipio' => $data['id_municipio'],
                               'id_medio_convocatoria' => $data['id_medio_convocatoria'],
                               'fecha' => $data['fecha'],
                               'hora_inicio' => $data['hora_inicio'],
                               'lugar' => $data['lugar'],
                               'id_organizacion_gpo' => $data['id_organizacion_gpo'],
                               'actividad_realizar' => $data['actividad_realizar'],
                               'itinerarios_previso' => $data['itinerarios_previso'],
                               'motivo_evento' => $data['motivo_evento'],
                               'quien_informa' => $data['quien_informa'],
                               'id_usuario' => $data['id_usuario'],
                               'resultados' => $data['resultados'],
                               'opecat_mcs_medios_convocatorias_medio_convocatoria' => $data['medio_convocatoria'],
                               'opecat_mcs_municipios_id_estado' => $data['id_estado'],
                               'opecat_mcs_municipios_Municipio' => $data['Municipio'],
                               'opecat_organizaciones_grupos_organizacion_grupo' => $data['organizacion_grupo'],
                               'opecat_organizaciones_grupos_fecha_creacion' => $data['fecha_creacion'],
                               'opecat_organizaciones_grupos_observaciones' => $data['observaciones'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mcs_agenda(id_agenda, id_municipio, id_medio_convocatoria, fecha, hora_inicio, lugar, id_organizacion_gpo, actividad_realizar, itinerarios_previso, motivo_evento, quien_informa, id_usuario, resultados)
                VALUES(:id_agenda, :id_municipio, :id_medio_convocatoria, :fecha, :hora_inicio, :lugar, :id_organizacion_gpo, :actividad_realizar, :itinerarios_previso, :motivo_evento, :quien_informa, :id_usuario, :resultados);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_agenda" => $this->id_agenda, ":id_municipio" => $this->id_municipio, ":id_medio_convocatoria" => $this->id_medio_convocatoria, ":fecha" => $this->fecha, ":hora_inicio" => $this->hora_inicio, ":lugar" => $this->lugar, ":id_organizacion_gpo" => $this->id_organizacion_gpo, ":actividad_realizar" => $this->actividad_realizar, ":itinerarios_previso" => $this->itinerarios_previso, ":motivo_evento" => $this->motivo_evento, ":quien_informa" => $this->quien_informa, ":id_usuario" => $this->id_usuario, ":resultados" => $this->resultados));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mcs_agenda
                   SET id_municipio=:id_municipio, id_medio_convocatoria=:id_medio_convocatoria, fecha=:fecha, hora_inicio=:hora_inicio, lugar=:lugar, id_organizacion_gpo=:id_organizacion_gpo, actividad_realizar=:actividad_realizar, itinerarios_previso=:itinerarios_previso, motivo_evento=:motivo_evento, quien_informa=:quien_informa, id_usuario=:id_usuario, resultados=:resultados
                WHERE id_agenda=:id_agenda;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_agenda" => $this->id_agenda, ":id_municipio" => $this->id_municipio, ":id_medio_convocatoria" => $this->id_medio_convocatoria, ":fecha" => $this->fecha, ":hora_inicio" => $this->hora_inicio, ":lugar" => $this->lugar, ":id_organizacion_gpo" => $this->id_organizacion_gpo, ":actividad_realizar" => $this->actividad_realizar, ":itinerarios_previso" => $this->itinerarios_previso, ":motivo_evento" => $this->motivo_evento, ":quien_informa" => $this->quien_informa, ":id_usuario" => $this->id_usuario, ":resultados" => $this->resultados));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>