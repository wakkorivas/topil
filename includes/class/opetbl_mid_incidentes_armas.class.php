<?php
/**
 *
 */
class OpetblMidIncidentesArmas
{
    public $id_arma; /** @Tipo: int(10), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_folio_incidente; /** @Tipo: int(10) unsigned zerofill, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_arma_rol; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_arma_tipo; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_arma_calibre; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_arma_marca; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $numero_serie; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $modelo; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $matricula; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si ?ste ocurre
    private $_conexBD; // objeto de conexi?n a la base de datos
    public $OpetblMidIncidentes; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidArmasMarcas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidArmasRoles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidArmasTipos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidArmasCalibres; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opetbl_mid_incidentes.class.php';
        require_once 'opecat_mid_armas_marcas.class.php';
        require_once 'opecat_mid_armas_roles.class.php';        
        require_once 'opecat_mid_armas_tipos_calibres.class.php';
        $this->OpetblMidIncidentes = new OpetblMidIncidentes();
        $this->OpecatMidArmasMarcas = new OpecatMidArmasMarcas();
        $this->OpecatMidArmasRoles = new OpecatMidArmasRoles();        
        $this->OpecatMidArmasTiposCalibres = new OpecatMidArmasTiposCalibres();
    }

    /**
    * Funci�n para obtener un el id maximo de la tabla
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getIdUltimaArma(){        
        $sql = "SELECT MAX(id_arma) as id
                  FROM opetbl_mid_incidentes_armas;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
    * Funci�n para obtener un registro espec�fico de la tabla
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getTotalArmas( $id_folio_incidente ){        
        $sql = "SELECT count(*) as id
                  FROM opetbl_mid_incidentes_armas 
                  WHERE id_folio_incidente = " . $id_folio_incidente;
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener un registro espec?fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz? con ?xito
     */
    public function select($id_arma)
    {
        $sql = "SELECT id_arma, id_folio_incidente, id_arma_rol, id_arma_tipo, id_arma_calibre, id_arma_marca, numero_serie, modelo, matricula
                FROM opetbl_mid_incidentes_armas
                WHERE id_arma=:id_arma;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_arma' => $id_arma));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_arma = $data['id_arma'];
            $this->id_folio_incidente = $data['id_folio_incidente'];
            $this->id_arma_rol = $data['id_arma_rol'];
            $this->id_arma_tipo = $data['id_arma_tipo'];
            $this->id_arma_calibre = $data['id_arma_calibre'];
            $this->id_arma_marca = $data['id_arma_marca'];
            $this->numero_serie = $data['numero_serie'];
            $this->modelo = $data['modelo'];
            $this->matricula = $data['matricula'];

            $this->OpetblMidIncidentes->select($this->id_folio_incidente);
            $this->OpecatMidArmasMarcas->select($this->id_arma_marca);
            $this->OpecatMidArmasRoles->select($this->id_arma_rol);            
            //$this->OpecatMidArmasTiposCalibres->select($this->id_arma_calibre);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_arma, a.id_folio_incidente, a.id_arma_rol, a.id_arma_tipo, a.id_arma_calibre, a.id_arma_marca, a.numero_serie, a.modelo, a.matricula,
                  b.id_folio_incidente, b.prioridad, b.fecha_hora_captura, b.fecha_incidente, b.dia, b.mes, b.annio, b.hora_incidente, b.id_estado, b.id_region, b.id_cuartel, b.id_municipio, b.direccion, b.localidad, b.colonia, b.calle, b.latitud, b.longitud, b.id_asunto, b.id_fuente, b.hechos_html, b.hechos_text, b.ip_usuario, b.id_usuario,
                  c.id_arma_marca, c.arma_marca,
                  d.id_arma_rol, d.arma_rol,
                  e.id_arma_tipo, e.id_arma_calibre,
                  f.id_arma_tipo, f.id_arma_calibre
                FROM opetbl_mid_incidentes_armas a
                 LEFT JOIN opetbl_mid_incidentes b ON a.id_folio_incidente=b.id_folio_incidente
                 LEFT JOIN opecat_mid_armas_marcas c ON a.id_arma_marca=c.id_arma_marca
                 LEFT JOIN opecat_mid_armas_roles d ON a.id_arma_rol=d.id_arma_rol
                 LEFT JOIN opecat_mid_armas_tipos_calibres e ON a.id_arma_tipo=e.id_arma_tipo
                 LEFT JOIN opecat_mid_armas_tipos_calibres f ON a.id_arma_calibre=f.id_arma_calibre";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_arma' => $data['id_arma'],
                               'id_folio_incidente' => $data['id_folio_incidente'],
                               'id_arma_rol' => $data['id_arma_rol'],
                               'id_arma_tipo' => $data['id_arma_tipo'],
                               'id_arma_calibre' => $data['id_arma_calibre'],
                               'id_arma_marca' => $data['id_arma_marca'],
                               'numero_serie' => $data['numero_serie'],
                               'modelo' => $data['modelo'],
                               'matricula' => $data['matricula'],
                               'opetbl_mid_incidentes_prioridad' => $data['prioridad'],
                               'opetbl_mid_incidentes_fecha_hora_captura' => $data['fecha_hora_captura'],
                               'opetbl_mid_incidentes_fecha_incidente' => $data['fecha_incidente'],
                               'opetbl_mid_incidentes_dia' => $data['dia'],
                               'opetbl_mid_incidentes_mes' => $data['mes'],
                               'opetbl_mid_incidentes_annio' => $data['annio'],
                               'opetbl_mid_incidentes_hora_incidente' => $data['hora_incidente'],
                               'opetbl_mid_incidentes_id_estado' => $data['id_estado'],
                               'opetbl_mid_incidentes_id_region' => $data['id_region'],
                               'opetbl_mid_incidentes_id_cuartel' => $data['id_cuartel'],
                               'opetbl_mid_incidentes_id_municipio' => $data['id_municipio'],
                               'opetbl_mid_incidentes_direccion' => $data['direccion'],
                               'opetbl_mid_incidentes_localidad' => $data['localidad'],
                               'opetbl_mid_incidentes_colonia' => $data['colonia'],
                               'opetbl_mid_incidentes_calle' => $data['calle'],
                               'opetbl_mid_incidentes_latitud' => $data['latitud'],
                               'opetbl_mid_incidentes_longitud' => $data['longitud'],
                               'opetbl_mid_incidentes_id_asunto' => $data['id_asunto'],
                               'opetbl_mid_incidentes_id_fuente' => $data['id_fuente'],
                               'opetbl_mid_incidentes_hechos_html' => $data['hechos_html'],
                               'opetbl_mid_incidentes_hechos_text' => $data['hechos_text'],
                               'opetbl_mid_incidentes_ip_usuario' => $data['ip_usuario'],
                               'opetbl_mid_incidentes_id_usuario' => $data['id_usuario'],
                               'opecat_mid_armas_marcas_arma_marca' => $data['arma_marca'],
                               'opecat_mid_armas_roles_arma_rol' => $data['arma_rol'],
                               'opecat_mid_armas_tipos_calibres_id_arma_calibre' => $data['id_arma_calibre'],
                               'opecat_mid_armas_tipos_calibres_id_arma_tipo' => $data['id_arma_tipo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }


    /**
     * Funci?n que controla la obtenci?n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci?n para obtener los registros m?nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {

        $sql = "SELECT a.id_arma, a.id_folio_incidente, ar.arma_rol, ati.arma_tipo, ac.arma_calibre, am.arma_marca,
                a.numero_serie, a.modelo, a.matricula
                FROM opetbl_mid_incidentes_armas a
                LEFT JOIN opecat_mid_armas_roles ar ON a.id_arma_rol=ar.id_arma_rol
                LEFT JOIN opecat_mid_armas_tipos ati ON a.id_arma_tipo=ati.id_arma_tipo
                LEFT JOIN opecat_mid_armas_calibres ac ON a.id_arma_calibre=ac.id_arma_calibre
                LEFT JOIN opecat_mid_armas_marcas am ON a.id_arma_marca=am.id_arma_marca";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_arma'            =>  $data['id_arma'],
                                'id_folio_incidente' =>  $data['id_folio_incidente'],
                                'arma_rol'           =>  $data['arma_rol'],
                                'arma_tipo'          =>  $data['arma_tipo'],
                                'arma_calibre'       =>  $data['arma_calibre'],
                                'arma_marca'         =>  $data['arma_marca'],
                                'numero_serie'       =>  $data['numero_serie'],
                                'modelo'             =>  $data['modelo'],
                                'matricula'          =>  $data['matricula'],
                                );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                FROM opetbl_mid_incidentes_armas a
                LEFT JOIN opecat_mid_armas_roles ar ON a.id_arma_rol=ar.id_arma_rol
                LEFT JOIN opecat_mid_armas_tipos ati ON a.id_arma_tipo=ati.id_arma_tipo
                LEFT JOIN opecat_mid_armas_calibres ac ON a.id_arma_calibre=ac.id_arma_calibre
                LEFT JOIN opecat_mid_armas_marcas am ON a.id_arma_marca=am.id_arma_marca";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }


    /**
     * Funcion para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el ?ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mid_incidentes_armas(id_folio_incidente, id_arma_rol, id_arma_tipo, id_arma_calibre, 
                    id_arma_marca, numero_serie, modelo, matricula)
                VALUES(:id_folio_incidente, :id_arma_rol, :id_arma_tipo, :id_arma_calibre, :id_arma_marca, 
                    :numero_serie, :modelo, :matricula);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_folio_incidente" => $this->id_folio_incidente, ":id_arma_rol" => $this->id_arma_rol, 
                                ":id_arma_tipo" => $this->id_arma_tipo, ":id_arma_calibre" => $this->id_arma_calibre, 
                                ":id_arma_marca" => $this->id_arma_marca, ":numero_serie" => $this->numero_serie, 
                                ":modelo" => $this->modelo, ":matricula" => $this->matricula));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            //$this->msjError = $sql;
            return false;
        }
    }

    /**
     * Funcion para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mid_incidentes_armas
                   SET id_folio_incidente=:id_folio_incidente, id_arma_rol=:id_arma_rol, id_arma_tipo=:id_arma_tipo, 
                   id_arma_calibre=:id_arma_calibre, id_arma_marca=:id_arma_marca, numero_serie=:numero_serie, 
                   modelo=:modelo, matricula=:matricula
                WHERE id_arma=:id_arma;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_arma" => $this->id_arma, ":id_folio_incidente" => $this->id_folio_incidente, 
                                ":id_arma_rol" => $this->id_arma_rol, ":id_arma_tipo" => $this->id_arma_tipo, 
                                ":id_arma_calibre" => $this->id_arma_calibre, ":id_arma_marca" => $this->id_arma_marca, 
                                ":numero_serie" => $this->numero_serie, ":modelo" => $this->modelo, 
                                ":matricula" => $this->matricula));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>