<?php
/**
 *
 */
class OpetblMpcDatosCriminales
{
    public $id_dato_criminal; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_perfil_criminal; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_grupo_delictivo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $cargo_ocupa; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $zona_operacion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $modus_operandis; /** @Tipo: varchar(200), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $buscado_por; /** @Tipo: varchar(200), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: varchar(400), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMpcGruposDelictivos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpetblMpcPerfilesCriminales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mpc_grupos_delictivos.class.php';
        require_once 'opetbl_mpc_perfiles_criminales.class.php';
        $this->OpecatMpcGruposDelictivos = new OpecatMpcGruposDelictivos();
        $this->OpetblMpcPerfilesCriminales = new OpetblMpcPerfilesCriminales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_dato_criminal)
    {
        $sql = "SELECT id_dato_criminal, id_perfil_criminal, id_grupo_delictivo, cargo_ocupa, zona_operacion, modus_operandis, buscado_por, observaciones
                FROM opetbl_mpc_datos_criminales
                WHERE id_dato_criminal=:id_dato_criminal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_dato_criminal' => $id_dato_criminal));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_dato_criminal = $data['id_dato_criminal'];
            $this->id_perfil_criminal = $data['id_perfil_criminal'];
            $this->id_grupo_delictivo = $data['id_grupo_delictivo'];
            $this->cargo_ocupa = $data['cargo_ocupa'];
            $this->zona_operacion = $data['zona_operacion'];
            $this->modus_operandis = $data['modus_operandis'];
            $this->buscado_por = $data['buscado_por'];
            $this->observaciones = $data['observaciones'];

            $this->OpecatMpcGruposDelictivos->select($this->id_grupo_delictivo);
            $this->OpetblMpcPerfilesCriminales->select($this->id_perfil_criminal);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_dato_criminal, a.id_perfil_criminal, a.id_grupo_delictivo, a.cargo_ocupa, a.zona_operacion, a.modus_operandis, a.buscado_por, a.observaciones,
                  b.id_grupo_delictivo, b.grupo_delictivo,
                  c.id_perfil_criminal, c.id_situacion, c.nombre, c.apellido_paterno, c.apellido_materno, c.alias, c.sexo, c.fecha_nacimineto, c.id_nacionalidad, c.lugar_origen, c.id_estado_civil, c.complexion, c.ocupacion, c.domicilio, c.estatura, c.Peso, c.cabello, c.colorPiel, c.ColorOjos
                FROM opetbl_mpc_datos_criminales a 
                 LEFT JOIN opecat_mpc_grupos_delictivos b ON a.id_grupo_delictivo=b.id_grupo_delictivo
                 LEFT JOIN opetbl_mpc_perfiles_criminales c ON a.id_perfil_criminal=c.id_perfil_criminal";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_dato_criminal' => $data['id_dato_criminal'],
                               'id_perfil_criminal' => $data['id_perfil_criminal'],
                               'id_grupo_delictivo' => $data['id_grupo_delictivo'],
                               'cargo_ocupa' => $data['cargo_ocupa'],
                               'zona_operacion' => $data['zona_operacion'],
                               'modus_operandis' => $data['modus_operandis'],
                               'buscado_por' => $data['buscado_por'],
                               'observaciones' => $data['observaciones'],
                               'opecat_mpc_grupos_delictivos_grupo_delictivo' => $data['grupo_delictivo'],
                               'opetbl_mpc_perfiles_criminales_id_situacion' => $data['id_situacion'],
                               'opetbl_mpc_perfiles_criminales_nombre' => $data['nombre'],
                               'opetbl_mpc_perfiles_criminales_apellido_paterno' => $data['apellido_paterno'],
                               'opetbl_mpc_perfiles_criminales_apellido_materno' => $data['apellido_materno'],
                               'opetbl_mpc_perfiles_criminales_alias' => $data['alias'],
                               'opetbl_mpc_perfiles_criminales_sexo' => $data['sexo'],
                               'opetbl_mpc_perfiles_criminales_fecha_nacimineto' => $data['fecha_nacimineto'],
                               'opetbl_mpc_perfiles_criminales_id_nacionalidad' => $data['id_nacionalidad'],
                               'opetbl_mpc_perfiles_criminales_lugar_origen' => $data['lugar_origen'],
                               'opetbl_mpc_perfiles_criminales_id_estado_civil' => $data['id_estado_civil'],
                               'opetbl_mpc_perfiles_criminales_complexion' => $data['complexion'],
                               'opetbl_mpc_perfiles_criminales_ocupacion' => $data['ocupacion'],
                               'opetbl_mpc_perfiles_criminales_domicilio' => $data['domicilio'],
                               'opetbl_mpc_perfiles_criminales_estatura' => $data['estatura'],
                               'opetbl_mpc_perfiles_criminales_Peso' => $data['Peso'],
                               'opetbl_mpc_perfiles_criminales_cabello' => $data['cabello'],
                               'opetbl_mpc_perfiles_criminales_colorPiel' => $data['colorPiel'],
                               'opetbl_mpc_perfiles_criminales_ColorOjos' => $data['ColorOjos'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

/**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT dc.id_dato_criminal,pc.id_perfil_criminal,pc.nombre,pc.apellido_paterno,pc.apellido_materno,
                gd.grupo_delictivo,dc.cargo_ocupa, dc.zona_operacion,dc.modus_operandis,dc.buscado_por,dc.observaciones
                FROM opetbl_mpc_datos_criminales AS dc
                LEFT JOIN opetbl_mpc_perfiles_criminales AS pc ON pc.id_perfil_criminal=dc.id_perfil_criminal
                JOIN opecat_mpc_grupos_delictivos as gd on dc.id_grupo_delictivo=gd.id_grupo_delictivo";

        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_dato_criminal' =>  $data['id_dato_criminal'],
                                'id_perfil_criminal' =>  $data['id_perfil_criminal'],
                                'nombre' =>  $data['nombre'],
                                'apellido_paterno' =>  $data['apellido_paterno'],
                                'apellido_materno,' =>  $data['apellido_materno,'],
                                'grupo_delictivo' =>  $data['grupo_delictivo'],
                                'cargo_ocupa' =>  $data['cargo_ocupa'],
                                'zona_operacion' =>  $data['zona_operacion'],
                                'modus_operandis' =>  $data['modus_operandis'],
                                'buscado_por' =>  $data['buscado_por'],
                                'observaciones' =>  $data['observaciones']
                                );
                                
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                FROM opetbl_mpc_datos_criminales AS dc
                LEFT JOIN opetbl_mpc_perfiles_criminales AS pc ON pc.id_perfil_criminal=dc.id_perfil_criminal
                JOIN opecat_mpc_grupos_delictivos as gd on dc.id_grupo_delictivo=gd.id_grupo_delictivo";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }



    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mpc_datos_criminales(id_dato_criminal, id_perfil_criminal, id_grupo_delictivo, cargo_ocupa, zona_operacion, modus_operandis, buscado_por, observaciones)
                VALUES(:id_dato_criminal, :id_perfil_criminal, :id_grupo_delictivo, :cargo_ocupa, :zona_operacion, :modus_operandis, :buscado_por, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dato_criminal" => $this->id_dato_criminal, ":id_perfil_criminal" => $this->id_perfil_criminal, ":id_grupo_delictivo" => $this->id_grupo_delictivo, ":cargo_ocupa" => $this->cargo_ocupa, ":zona_operacion" => $this->zona_operacion, ":modus_operandis" => $this->modus_operandis, ":buscado_por" => $this->buscado_por, ":observaciones" => $this->observaciones));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mpc_datos_criminales
                   SET id_perfil_criminal=:id_perfil_criminal, id_grupo_delictivo=:id_grupo_delictivo, cargo_ocupa=:cargo_ocupa, zona_operacion=:zona_operacion, modus_operandis=:modus_operandis, buscado_por=:buscado_por, observaciones=:observaciones
                WHERE id_dato_criminal=:id_dato_criminal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dato_criminal" => $this->id_dato_criminal, ":id_perfil_criminal" => $this->id_perfil_criminal, ":id_grupo_delictivo" => $this->id_grupo_delictivo, ":cargo_ocupa" => $this->cargo_ocupa, ":zona_operacion" => $this->zona_operacion, ":modus_operandis" => $this->modus_operandis, ":buscado_por" => $this->buscado_por, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>