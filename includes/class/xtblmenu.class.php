<?php
/**
 *
 */
class Xtblmenu
{
    public $id_menu; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $menu; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $url; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $icono; /** @Tipo: varchar(254), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $stat; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    public $id_padre; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $orden; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    //public $Xtblmenu; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        //require_once 'xtblmenu.class.php';
        //$this->Xtblmenu = new Xtblmenu();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_menu)
    {
        $sql = "SELECT id_menu, menu, url, icono, stat, id_padre, orden
                FROM xtblmenu
                WHERE id_menu=:id_menu;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_menu' => $id_menu));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_menu = $data['id_menu'];
            $this->menu = $data['menu'];
            $this->url = $data['url'];
            $this->icono = $data['icono'];
            $this->stat = $data['stat'];
            $this->id_padre = $data['id_padre'];
            $this->orden = $data['orden'];

            //$this->Xtblmenu->select($this->id_padre);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_menu, a.menu, a.url, a.icono, a.stat, a.id_padre, a.orden,
                  b.id_menu, b.menu, b.url, b.icono, b.stat, b.id_padre, b.orden
                FROM xtblmenu a 
                 LEFT JOIN xtblmenu b ON a.id_padre=b.id_menu";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_menu' => $data['id_menu'],
                               'menu' => $data['menu'],
                               'url' => $data['url'],
                               'icono' => $data['icono'],
                               'stat' => $data['stat'],
                               'id_padre' => $data['id_padre'],
                               'orden' => $data['orden'],
                               'xtblmenu_id_menu' => $data['id_menu'],
                               'xtblmenu_menu' => $data['menu'],
                               'xtblmenu_url' => $data['url'],
                               'xtblmenu_icono' => $data['icono'],
                               'xtblmenu_stat' => $data['stat'],
                               'xtblmenu_orden' => $data['orden'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xtblmenu(id_menu, menu, url, icono, stat, id_padre, orden)
                VALUES(:id_menu, :menu, :url, :icono, :stat, :id_padre, :orden);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_menu" => $this->id_menu, ":menu" => $this->menu, ":url" => $this->url, ":icono" => $this->icono, ":stat" => $this->stat, ":id_padre" => $this->id_padre, ":orden" => $this->orden));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xtblmenu
                   SET menu=:menu, url=:url, icono=:icono, stat=:stat, id_padre=:id_padre, orden=:orden
                WHERE id_menu=:id_menu;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_menu" => $this->id_menu, ":menu" => $this->menu, ":url" => $this->url, ":icono" => $this->icono, ":stat" => $this->stat, ":id_padre" => $this->id_padre, ":orden" => $this->orden));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }

    public function getMenuTree($tamMax=50, $xOptSelected=0)
    {
        $qSql = $this->_conexBD->prepare("SELECT * 
                                            FROM xtblmenu
                                            WHERE (url = '#' OR url IS NULL) AND stat = 1 AND id_padre IS NULL
                                            ORDER BY orden;");
        if( $qSql->execute() ){
            $qSql->setFetchMode(PDO::FETCH_ASSOC);
            while( $f = $qSql->fetch() ){
                if( strlen($f['menu']) > $tamMax ){
                    $descrip = substr($f['menu'], 0, $tamMax - 3) . "...";
                    $xhint = 'title="' . $f['menu'] . '"';
                }
                else{
                    $descrip = $f['menu'];
                    $xhint = '';
                } 
                $html .= '<option value="' . $f['id_menu'] . '" ' . $xhint . '>' . $descrip . '</option>';
                $html .= $this->_opcion($f['id_menu'], 1, $xOptSelected, $tamMax);
            }        
        }
        else{
            $aryError = $qSql->errorInfo();
            $this->xERROR = $aryError[2];
            return false;
        }
        
        return $html;
    }

    private function _opcion($id_menu, $xNivel, $xOptSelected, $tamMax)
    {
        $qCount = $this->_conexBD->prepare("SELECT COUNT(*) 
                                            FROM xtblmenu cm 
                                            WHERE cm.stat = 1 AND cm.id_padre = :xid_menu;");
        $qCount->execute( array(":xid_menu" => $id_menu) );
        if( $qCount->fetchColumn() > 0 ){
            $qSql = $this->_conexBD->prepare("SELECT cm.id_menu, cm.menu            
                                                FROM xtblmenu cm 
                                                WHERE cm.stat = 1 AND cm.id_padre = :xid_menu 
                                                ORDER BY cm.orden;");
            $qSql->execute( array(":xid_menu" => $id_menu) );
        
            $xNivel++;
            $html = '';
            $qSql->setFetchMode(PDO::FETCH_ASSOC);
            while( $f1 = $qSql->fetch() ){
                //--- control de la tabulacion...
                $xtab = $this->_xtabulador($xNivel);               
                //--- Tama�o del texto a mostrar en el option...
                $xtam_maximo = $tamMax - (($xNivel-1) * 5);
                $tam_subcadena = ($xtam_maximo - 3) + $xNivel;
                if( (strlen($f1['menu']) > $xtam_maximo) && (strlen($f1['menu']) > $tam_subcadena) ){
                    $descrip = substr($f1['menu'], 0, $tam_subcadena)."...";
                    $xhint = 'title="' . $f1['menu'] . '"';
                }            
                else{
                    $descrip = $f1['menu'];
                    $xhint = '';
                }
                
                $xSelected = "";
                if( $xOptSelected == $f1['id_menu'] )
                    $xSelected = "selected";
                $html .= '<option value="' . $f1['id_menu'] . '" ' . $xhint . ' ' . $xSelected . '>' . $xtab . $descrip . '</option>';
                
                $html .= $this->_opcion($f1['id_menu'], $xNivel, $xOptSelected, $tamMax);
            }
        }
        
        return $html;
    }

    private function _xtabulador($nivel)
    {
        $total = (int)($nivel - 1) * 6;                        
        $tab = "";
        for( $idx=0; $idx<$total; $idx++ )
            $tab .= "&nbsp;";
        return $tab;
    }

}


?>