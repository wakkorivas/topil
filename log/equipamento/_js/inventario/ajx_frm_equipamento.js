$(document).ready(function(){
    
        $('#frmAlta').validate({
        rules:{                        
            cbxMarca:{
                required: true,
                min: 1,
            },
            cbxTipo:{
                required: true,
                min: 1,
            },    
            cbxEntidad:{
                required: true,
                min: 1,
            },     
            cbxDependencia:{
                required: true,
                min: 1,
            },             
            txtInventario: 'required',            
            txtSerie: 'required',              
            txtModelo: 'required',                        
    	},
    	messages:{
            cbxMarca:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxTipo:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxEntidad:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxDependencia:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    		txtInventario: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtSerie: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtModelo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
        
     }); 
    
});