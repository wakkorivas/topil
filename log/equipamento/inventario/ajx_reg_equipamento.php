<?php
/**
 * Complemento ajax para guardar los datos del armamento 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/logtbl_equipamento.class.php';
    include $path . 'includes/class/logtbl_eq_alta.class.php';
    include $path . 'includes/class/config/system.class.php';
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objEq   = new LogtblEquipamento();
    $objAlta = new LogtblEqAlta();        
    
    // Datos del armamento
    if( $_POST["dtTypeOper"] == 1 ){
        $objEq->id_equipamento = $_POST["id_equipamento"];     
    }
    $objEq->id_marca = $_POST["cbxMarca"];    
    $objEq->id_tipo = $_POST["cbxTipo"];
    $objEq->id_entidad = $_POST["cbxEntidad"];
    $objEq->id_dependencia = $_POST["cbxDependencia"];
    $objEq->inventario = strtoupper( $_POST["txtInventario"] );
    $objEq->serie = strtoupper( $_POST["txtSerie"] );
    $objEq->modelo = strtoupper( $_POST["txtModelo"] ); 
    $objEq->id_estatus = 1;
    
    if( $_POST["dtTypeOper"] == 1 ){
        $result = $objEq->update() ;  
        $id = $objEq->id_equipamento;
    }elseif( $_POST["dtTypeOper"] == 0 ){
        $result = $objEq->insert();  
        $id = $result;
    } 
    
    $datos = $objEq->selectAllGrid('a.id_estatus = 1 and a.id_equipamento=' . $id, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'logtbl_equipamento', $id, "Ins");
        $objAlta->id_equipamento = $id;
        $objAlta->usuario   = $objUsr->idUsr;
        $objAlta->insert();
        $ajx_datos['rslt']  = true;
        
        // Se obtienen las ultimas altas de equipamento para el grid principal de resultados.
        $datos = $objEq->selectAll("a.id_equipamento='" . strtoupper( $_POST["id_equipamento"] ) . "'", "a.id_equipamento Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Equip-tbBody">';
        foreach( $datos As $reg => $dato ){            
                
                $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_equipamento"] . '">';
           			//--------------------- Impresion de datos ----------------------//
                    $html .= '<td style="text-align: center; width: 3%;">';
                       $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                    $html .= '</td>';
                    $html .= '<td style="text-align: center; width: 17%;">' . $dato["marca"] . '</td>';               
                    $html .= '<td style="text-align: center; width: 15%;">' . $dato["tipo"] . '</td>';
                    $html .= '<td style="text-align: center; width: 20%;">' . $dato["inventario"] . '</td>';
                    $html .= '<td style="text-align: center; width: 15%;">' . $dato["serie"] . '</td>';
    				$html .= '<td style="text-align: center; width: 15%;">' . $dato["modelo"] . '</td>';
                    $html .= '<td style="text-align: center; width: 15%;">';
                    $html .= '  <a href="#" class="lnkBtnOpcionGrid" id="btnModifica" title="Modificar caracteristicas del equipamento..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';                                
                    $html .= '  <a href="#" class="lnkBtnOpcionGrid" id="btnBaja" title="Dar de baja equipamento..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                    $html .= '</td>';
                 	//---------------------------------------------------------------//
           		$html .= '</tr>'; 
        }        
        $html .= '</table>';
        
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos["total"] = $totalReg;
        //$ajx_datos['campos']= $objArm->matricula . "-" . $objArm->id_serie . "-" . $objArm->id_loc . "-" . $objArm->id_foliod . "-" .  $objArm->id_situacion . "-" . $objArm->id_propietario . "-" . $objArm->id_estado . "-" . $objArm->id_estatus . "-" . $objArm->id_motivo_movimiento . "-" . $objArm->id_marca . "-->" . $_POST["dtTypeOper"];
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos["total"] = 0;
        $ajx_datos['error'] = $objEq->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>