$(document).ready(function(){
    //codigo para validar campos
    $('#frmAlta').validate({
        rules:{
            // Datos Personales
            txtMatricula: 'required',
            txtSerie: 'required',
            cbxLicencia:{
                required: true,
                min: 1,
            },
            cbxTipo:{
                required: true,
                min: 1,
            },
            cbxClase:{
                required: true,
                min: 1,
            },
            cbxCalibre:{
                required: true,
                min: 1,
            },
            cbxModelo:{
                required: true,
                min: 1,
            },
            cbxMarca:{
                required: true,
                min: 1,
            },
            cbxFolioc:{
                required: true,
                min: 1,
            },
            cbxFoliod:{
                required: true,
                min: 1,
            },
            cbxPropietario:{
                required: true,
                min: 1,
            },
    	},
    	messages:{
            txtMatricula: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtSerie: '<span class="ValidateError" title="Este campo es obligatorio"></span>',            
            cbxLicencia:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },             
            cbxTipo:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxClase:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxCalibre:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxModelo:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMarca:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxFolioc:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxFoliod:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxPropietario:{
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },

    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    
    
});