<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_arm_asignacion.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario();        
    $objAsig = new LogtblArmAsignacion();
    
    // Datos del armamento
    $objAsig->matricula = strtoupper( $_POST["txtMatricula"] );
    $objBaja->oficio = strtoupper( $_POST["txtOficio"] );
    $objBaja->fecha_oficio = $objSys->convertirFecha( $_POST["txtFechaOficio"], "yyyy-mm-dd" );
    $objBaja->id_motivo= $_POST["cbxMotivo"];
    //$objBaja->fecha_baja = $objSys->convertirFecha( $_POST["txtFechaBaja"], "yyyy-mm-dd" );
    $objBaja->id_usuario = $objUsr->idUsr;
        
    $result = $objBaja->insert();            
    
    if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'logtbl_arm_baja', $objBaja->matricula, "Ins");
        $ajx_datos['rslt']  = true;
        
        $ajx_datos['html']  = utf8_encode($html);            
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';  
        $ajx_datos['campos']= $objBaja->matricula . "-" .$objBaja->oficio. "-" .$objBaja->fecha_oficio . "-" .$objBaja->id_motivo. "-" .$objBaja->fecha_baja . "-" .$objBaja->id_usuario;
        $ajx_datos['error'] = $objBaja->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>