<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_datos_personales.class.php';
$objDatPer = new AdmtblDatosPersonales();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Control de Armamento',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<link type="text/css" rel="stylesheet" href="log/loc/_css/font-awesome410/css/font-awesome.css" />',
                                   '<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="log/loc/_js/expediente/index.js"></script>',
                                   '<script type="text/javascript" src="log/loc/_js/radioButton.js"></script>',
                                   '<link type="text/css" rel="stylesheet" href="log/loc/_css/locStyles.css" />'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 60%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 40%;">
                    <!-- Botones de opci�n... 
                    <a href="index.php?m=<?php //echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('persona_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Ver la portaci&oacute;n de armamento...">
                        <img src="<?php //echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>
                    -->
                    <!--
                    <a href="index.php?m=<?php //echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('form_ficha_crim');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php //echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                    -->
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Personal" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch" border=0>
                <tr>
                    <td style="width: 25%;">Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                    <td style="width: 10%; text-align: left;">
                        <input type="radio" id="rad1" name="criterio" class="classCriterio" value="" checked="true" /> 
                        <label for="rad1">
                            <span class="fa-stack">
                                <i class="fa fa-dot-circle-o fa-stack-1x"></i>
                                <i class="fa fa-circle fa-stack-1x"></i>
                            </span>
                            NINGUNO
                        </label>
                    </td>
                    <td style="width: 10%;">
                        <input type="radio" id="rad2" name="criterio" class="classCriterio" value="1" /> 
                        <label for="rad2">
                            <span class="fa-stack">
                                <i class="fa fa-dot-circle-o fa-stack-1x"></i>
                                <i class="fa fa-circle fa-stack-1x"></i>
                            </span>
                            COMPLETO
                        </label>
                    </td>
                    <td style="width: 10%;">
                        <input type="radio" id="rad3" name="criterio" class="classCriterio" value="2" /> 
                        <label for="rad3">
                            <span class="fa-stack">
                                <i class="fa fa-dot-circle-o fa-stack-1x"></i>
                                <i class="fa fa-circle fa-stack-1x"></i>
                            </span>
                            INCOMPLETO
                        </label>
                    </td>
                    <td style="width: 15%;">
                        <input type="radio" id="rad4" name="criterio" class="classCriterio" value="0" /> 
                        <label for="rad4">
                            <span class="fa-stack">
                                <i class="fa fa-dot-circle-o fa-stack-1x"></i>
                                <i class="fa fa-circle fa-stack-1x"></i>
                            </span>
                            SIN DOCUMENTACION
                        </label>
                    </td>
                    <td style="width: 30%;"></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ESPECIALIDAD</th>
                    <th style="width: 17%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">ADSCRIPCI�N</th>
                    <th style="width: 15%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <!-- CONTENEDOR PARA LA EL FORMULARIO DE ARMAS ASIGNADAS ------------- -->
    <div id="dvExp" style="display: none;" title="Expediente LOC">    
        <div id="dvExpCont" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 400px; min-width: 650px; overflow: hidden;"></div>        
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('log/loc/expediente/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlExp" value="<?php echo $objSys->encrypt('log/loc/expediente/ajx_frm_exp.php');?>" />    
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>