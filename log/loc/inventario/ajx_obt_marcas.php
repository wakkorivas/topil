<?php
/**
 * Complemento del llamado ajax para listar las marcas dentro de un combobox. 
 * @param int id_clase, recibido por el m�todo GET.
 * @param int id_modelo, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/logcat_arm_marca.class.php';
    $objMar = new LogcatArmMarca();
    
    $datos = '<option value="0"></option>';
    if ( ($_GET["id_modelo"] != 0) && ($_GET["id_clase"] != 0) ){
        $datos .= $objMar->shwMarcas( 0, $_GET["id_clase"], $_GET['id_modelo'] );
    }
    if (empty($objMar->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objMar->msjError;        
    }
    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>