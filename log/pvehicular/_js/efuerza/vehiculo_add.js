$(document).ready(function(){ 
    // Barra de opciones flotante
    var barraOp = $('#dvBarraOpciones');
    var topBar = $(window).height() - (barraOp.height() + 7);
    var leftBar = (($(window).width() / 2) - (barraOp.width() / 2));
    var url_dir_tmp = xDcrypt($('#hdnUrlDirTemp').val());
    var url_upload = xDcrypt($('#hdnUrlUpload').val());
    
    barraOp.css({
        top: topBar,
        left: leftBar,
    });
    $(window).resize(function(){
        var topBar = $(this).height() - 60;
        var leftBar = (($(this).width() / 2) - (barraOp.width() / 2));
        barraOp.css({
            top: topBar,
            left: leftBar,
        });
    });
       
    //-- Script para cargar la Fotograf�a del frente...
    new AjaxUpload('#dvFotoFrente', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoFrente").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoFrente").val(result.file);
                $("#dvFotoFrente").empty();
                $("#dvFotoFrente").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a derecha...
    new AjaxUpload('#dvFotoDer', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoDer").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoDer").val(result.file);
                $("#dvFotoDer").empty();
                $("#dvFotoDer").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a izquierda...
    new AjaxUpload('#dvFotoIzq', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoIzq").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoIzq").val(result.file);
                $("#dvFotoIzq").empty();
                $("#dvFotoIzq").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a posterior...
    new AjaxUpload('#dvFotoPost', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoPost").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoPost").val(result.file);
                $("#dvFotoPost").empty();
                $("#dvFotoPost").append(imgHtml);
            }
        }	
    });
    
    //-- Script para cargar la Fotograf�a interior...
    new AjaxUpload('#dvFotoInt', {
        action: url_upload,        
		onSubmit : function(file, ext){
  		    if (! (ext && /^(jpg)$/.test(ext))){
  			   // extensiones permitidas
  			   shwError('Solo se permiten imagenes (.jpg)', 350);
  			   // cancela upload
  			   return false;
  		    } else {
  			   $("#dvFotoInt").html('<span class="dvLoading" style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
  		    }
        },
		onComplete: function(file, response){  
            var result = $.parseJSON(response);
            if( !result.rslt ){
                shwError(result.error, 450);
            } else{
                var imgHtml = '<img src="' + url_dir_tmp + result.file + '" />';
                $("#hdnFotoInt").val(result.file);
                $("#dvFotoInt").empty();
                $("#dvFotoInt").append(imgHtml);
            }
        }	
    });
    
    // M�scara de los campos tel�fonos
    
    //$('#txtNumSerie').mask('99aaa99999a99999999');
    $('#txtNumEco').mask('99999999');
    $('#txtPlacas').mask('aaa9999');
    $('#txtNumCilindros').mask('9');
    
	$('#txtFechaRes').datepicker({
        yearRange: '2010:2030',
    });
        
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNumMotor').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtPoliza').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtInciso').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });

    $('#frmRegistro').validate({
        rules:{
            //condiciones del vehiculo
            cbxClasificacion:{
                required: true,
                min: 1,
            },
            cbxMarca:{
                required: true,
                min: 1,
            },
            cbxCategoria:{
                required: true,
                min: 1,
            },                      
            txtNumSerie: 'required',            
            txtNumEco: 'required',              
            txtPlacas: 'required',
			rbnNumPlacas: 'required',			            
            cbxEdoFisico:{
                required: true,
                min: 1,
            },
            cbxSituacion:{
                required: true,
                min: 1,
            },
            cbxModelo:{
                required: true,
                min: 1,
            },
           cbxTransmision:{
                required: true,
                min: 1,
            },
            cbxColor:{
                required: true,
                min: 1,
            },                        
    	},
    	messages:{
            cbxClasificacion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxMarca:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxCategoria:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    		txtNumSerie: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtNumEco: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtPlacas: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            rbnNumPlacas: '<span class="ValidateError" title="Este campo es obligatorio"></span>',             
            cbxEdoFisico:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },  
            cbxSituacion:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxModelo:{
                requiered: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
            cbxTransmision:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },            
            cbxColor:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            },
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
        
     });
		
	// Control para la carga din�mica de 
    $('#cbxClasificacion').change(function(){
        obtenerTipos( 'cbxTipo', $(this).val() );  
    });
       
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida)
            $('#frmRegistro').submit();
    });    
});

function obtenerTipos(contenedor, id_clasificacion){	
    $.ajax({
        url: xDcrypt($('#hdnUrlTipo').val()),
        data: {'id_clasificacion': id_clasificacion },
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando tipos...</option>');
			obtenerMarcas( 'cbxMarca', id_clasificacion )
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}
function obtenerMarcas(contenedor, id_clasificacion){
    $.ajax({
        url: xDcrypt($('#hdnUrlMarca').val()),
        data: {'id_clasificacion': id_clasificacion },
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando marcas...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}