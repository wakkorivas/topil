var xGrid;
$(document).ready(function(){    
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Vehiculos').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Vehiculos').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    
    //-- Definici�n del dialog como formulario para la b�squeda de clientes....
    $('#dvListaMtto').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 500,
        height: 300,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });

    //-- Controla el bot�n para mirar la lista de mantenimientos...
    $(".btnListaMtto").live("click", function(e){               
        var ID = $(this).attr("id");                      
        $.ajax({
            url: xDcrypt( $('#hdnUrlLista').val() ),
            data: {'id_vehiculo': ID },
            dataType: 'json',
            async: true,
            cache: false,            
            success: function (xdata) {
                    $('#dvListMtto').html(xdata.html);
                    $('#dvListaMtto').dialog('open');
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });      

});