<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_vehiculos.class.php';
$objDatPer = new LogtblVehiculos();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="log/pvehicular/_js/efuerza/index.js"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opci�n... -->  
                    <!-- ver la lista de siniestrados-->                                                                              
                    <a href="#" id="btnLstSin" class="Tool-Bar-Btn" style="" title="Lista de vehiculos siniestrados">
                        <img src="<?php echo PATH_IMAGES;?>icons/lst_sin24.png" alt="" style="border: none;" /><br />Lista Sin.
                    </a> 
                    <!-- ver la lista de bajas-->                                                                              
                    <a href="#" id="btnLstBajas" class="Tool-Bar-Btn" style="" title="Lista de bajas de vehiculos">
                        <img src="<?php echo PATH_IMAGES;?>icons/lst_bajas24.png" alt="" style="border: none;" /><br />Lista Bajas
                    </a>                    
                    <!-- agregar un nuevo registro-->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('vehiculo_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo vehiculo...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>
                    <!-- reportes -->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('form_ficha_crim');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Vehiculos" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtSearch" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>                      
                    <th style="width: 13%;" class="xGrid-tbCols-ColSortable">SERIE</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">PLACAS</th>
                    <th style="width: 14%;" class="xGrid-tbCols-ColSortable">N&Uacute;MERO ECON&Oacute;MICO</th>  
                    <th style="width: 18%;" class="xGrid-tbCols-ColSortable">CLASIFICACI&oacute;N</th>
                    <th style="width: 18%;" class="xGrid-tbCols-ColSortable">MARCA</th>                   
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">TIPO</th>
                    <th style="width: 12%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody"></div>                            
    </div>
    <!-- CONTENEDOR PARA LISTA DE BAJAS DE VEHICULOS ------------- -->
    <div id="dvLstBajas" style="display: none;" title="Lista de bajas de vehiculos">            
            
        <div id="dvGrid-LstBajas" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbSearch">
                    <tr>
                        <td>Buscar: <input type="text" name="txtSearch" size="25" value="<?php if( isset($_SESSION['xSearchGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                    </tr>
                </table>
                <table class="xGrid-tbCols">
                    <tr>                          
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">SERIE</th>      
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">CLASIFICACION</th>  
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">MARCA</th>                        
                        <th style="width: 20%;" class="xGrid-tbCols-ColSortable">N&uacute;MERO DE LOTE</th>         
                        <th style="width: 20%;" class="xGrid-tbCols-ColSortable">MOTIVO BAJA</th>                                                    
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">FECHA BAJA</th>                        
                    </tr>
                </table>
            </div>
            <div id="xGridBodyBajas" class="xGrid-dvBody"></div>        
        </div>                
    </div>
    <!-- CONTENEDOR PARA LISTA DE VEHICULOS SINIESTRADOS ------------- -->
    <div id="dvLstSin" style="display: none;" title="Lista de vehiculos siniestrados">            
            
        <div id="dvGrid-LstSin" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbSearch">
                    <tr>
                        <td>Buscar: <input type="text" name="txtSearch" size="25" value="<?php if( isset($_SESSION['xSearchGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                    </tr>
                </table>
                <table class="xGrid-tbCols">
                    <tr>                          
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">N&Uacute;M.SERIE</th>      
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">N&Uacute;M.ECO.</th>  
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">MARCA</th>                        
                        <th style="width: 20%;" class="xGrid-tbCols-ColSortable">TIPO</th>         
                        <th style="width: 20%;" class="xGrid-tbCols-ColSortable">N&Uacute;M.SINIESTRO</th>                                                    
                        <th style="width: 15%;" class="xGrid-tbCols-ColSortable">FECHA SINIESTRO</th>                        
                    </tr>
                </table>
            </div>
            <div id="xGridBodySin" class="xGrid-dvBody"></div>        
        </div>                
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlObtLstBajas" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_lst_bajas.php');?>" />
    <input type="hidden" id="hdnUrlObtLstSin" value="<?php echo $objSys->encrypt('log/pvehicular/efuerza/ajx_obt_lst_sin.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>