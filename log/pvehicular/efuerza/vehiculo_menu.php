<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_vehiculos.class.php';
$objDatVeh = new LogtblVehiculos();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <!-- Botones de opci�n... -->                    
                    <a href="index.php" id="xRegresar" class="Tool-Bar-Btn" style="" title="Regresar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $_SESSION['xIdVehiculo'] = ($_GET['id']) ? $objSys->decrypt($_GET['id']) : $_SESSION['xIdVehiculo'];
    $objDatVeh->select($_SESSION['xIdVehiculo']);
    ?>    
    
    <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 600px;">
        <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Opciones del Veh&iacute;culo</span>
        
        <table class="tbForm-Data" style="width: 100%">
            <tr>
                <td id="tdFoto" rowspan="10" style="text-align: center; vertical-align: middle;">
                    <img src="imgs/policia.jpg" alt="foto" style="border: 3px solid gray; border-radius: 5px;" />
                    <p><?php echo $objDatVeh->marca .' ' . $objDatVeh->tipo;?></p>
                    <p><?php echo $objDatVeh->modelo;?></p>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="#" class="Tool-Bar-Btn" style="height: 30px; min-width: 200px;">Caracter&iacute;sticas</a>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="#" class="Tool-Bar-Btn" style="height: 30px; min-width: 200px;">Datos particulares ( Resguardo )</a>
                </td>
            </tr>            
            <tr>
                <td style="text-align: center;">
                    <a href="#" class="Tool-Bar-Btn" style="height: 30px; min-width: 200px;">Detalles</a>
                </td>
            </tr>
        </table>
    </div>
    
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>