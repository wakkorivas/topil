$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad para el Slider **************/
/********************************************************************/
$('#myslideshow').bjqs({
        width : 550,
        height :300,
        // animation values
        animtype : 'slide', // accepts 'fade' or 'slide'
        animduration : 450, // how fast the animation are
        animspeed : 3000, // the delay between each slide
        automatic : true, // automatic
        
        // control and marker configuration
        showcontrols : true, // show next and prev controls
        centercontrols : true, // center controls verically
        nexttext : '>>', // Text for 'next' button (can use HTML)
        prevtext : '<<', // Text for 'previous' button (can use HTML)
        showmarkers : true, // Show individual slide markers
        centermarkers : true, // Center markers horizontally
        // interaction values
        keyboardnav : true, // enable keyboard navigation
        hoverpause : true, // pause the slider on hover
        responsive : true
    });


/********************************************************************/
/***********  Configuracion del editor de texto TINYMCE  ************/
/********************************************************************/




/********************************************************************/
/** Convierte a may�sculas el contenido de los textbox y textarea. **/
/********************************************************************/




/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/

/***** Aplicaci�n de las reglas de validaci�n de los campos del formulario *****/
 

/***** Fin del document.ready *****/
});