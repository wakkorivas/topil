<?php
/**
 * Complemento del llamado ajax para mostrar o servir un archivo multimedia. 
 * @param int id, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    //header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/opetbl_mid_incidentes_multimedia.class.php';
    $objMult = new OpetblMidIncidentesMultimedia();
    
    $id_multimedia = ( isset($_POST["id"]) ) ? $_POST["id"] : $_GET["id"];    
    $objMult->select($id_multimedia);
    $ext = $objMult->OpecatMidMultimediaTipo->multimedia_tipo;
    
    $path_exped = 'ope/mid/_multimedia/';
    $archivo = $path_exped . $_SESSION["xIdIncidente"] . '/' . $_SESSION["xIdIncidente"] . '_' . $id_multimedia . '.' . $ext;
    
    switch($ext){
        case 'jpg':        
        case 'png':
        case 'bmp':
        case 'gif':
        case 'jpeg':
            $html = '<img src="' . $archivo . '" alt="' . $objMult->archivo . '" style="height: auto; max-width: 950px;" />';
            echo $html;
            break;
        case 'mp4':
        case 'ogg':
            $html = '<video controls style="height: auto; max-width: 950px;">
                        <source src="' . $archivo . '" type=\'video/mp4; codecs="avc1.4D401E, mp4a.40.2"\' />
                        <br />Lo sentimos, pero su navegador no soporta la reproducci�n del archivo multimedia seleccionado, pero puede <a href="' . $archivo . '">descargar el archivo</a> para verlo de manera local
                     </video>';
            echo $html;
            break;        
        case 'mp3': 
            $html = '<audio id="ctrlAudioHTML5" controls>                         
                         <source src="' . $archivo . '" type="audio/mpeg">
                         <object id="ctrlAudio1" classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
                            <param name="FileName" value="' . $archivo . '" />
                            <param name="autoplay" value="false" />
                            <object id="ctrlAudio2 type="audio/mpeg" data="' . $archivo . '">
                                <param name="controller" value="true" />
                                <param name="autoplay" value="false" />
                                <br />Lo sentimos, pero su navegador no soporta la reproducci�n del archivo multimedia seleccionado, pero puede <a href="' . $archivo . '">descargar el archivo</a> para verlo de manera local
                            </object>
                        </object>
                     </audio>';
            echo $html;
            break;
        case 'pdf': 
            $html = '<object id="objPdf" type="application/pdf" data="' . $archivo . '" class="" style="height: ' . $_POST["h"] . 'px; width: 100%;">
                        alt: <a href="' . $archivo . '">Ficha.pdf</a>
                    </object>';
            echo $html;
            break;
        case '3gp': 
        case 'mpeg':
        case 'avi':
        case 'wmv':
        case 'flv':
        case 'amr':
        case 'doc':
        case 'docx':
        case 'xls':
        case 'xlsx':
        case 'ppt':
        case 'pptx':
        case 'pps':
        case 'ppsx':
            $archivo = '../_multimedia/' . $_SESSION["xIdIncidente"] . '/' . $_SESSION["xIdIncidente"] . '_' . $id_multimedia . '.' . $ext;
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-disposition: attachment; filename=" . $objMult->archivo);
            header("Content-Transfer-Encoding: binary");
            readfile($archivo);
            break;
    }
} else {
    echo "Error de Sesi�n...";
}
?>