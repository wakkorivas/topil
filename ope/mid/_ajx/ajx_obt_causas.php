<?php
/**
 * Complemento del llamado ajax para listar las causas dentro de un combobox. 
 * @param int id_clase, recibido por el m�todo GET.
 * @param int id_modelo, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/opecat_mid_personas_roles_causas.class.php';
    $objRolCausa = new OpecatMidPersonasRolesCausas();
    
    if ( $_GET["id_rol"] != 0 ){
        $datos = $objRolCausa->getCat_mid_Personas_Roles_Causas(0, $_GET["id_rol"]);
    }
    if (empty($objRolCausa->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objRolCausa->msjError;        
    }
        
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>