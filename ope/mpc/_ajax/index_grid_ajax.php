<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par?metros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una b?squeda.
 * @param int colSort, contiene el ?ndice de la columna por la cual se ordenar?n los datos.
 * @param string typeSort, define el tipo de ordenaci?n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n?mero de fila por la cual inicia la paginaci?n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar?n en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mpc_perfiles_criminales.class.php';
    $objSys = new System();
    $objPerfiles = new OpetblMpcPerfilesCriminales();

    //--------------------- Recepci?n de par?metros --------------------------//
    // B?squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        
        $sql_where = 'pc.id_perfil_criminal LIKE ? '
                   . 'OR pc.nombre LIKE ? '
                   . 'OR pc.apellido_paterno LIKE ? '
                   . 'OR pc.apellido_materno LIKE ? '
                   . 'OR pc.sexo LIKE ? '
                   . 'OR pc.alias LIKE ? '
                   . 'OR gd.grupo_delictivo LIKE ?'
                   . 'OR sit.situacion LIKE ? '
                   . 'OR nal.nacionalidad LIKE ? '                   
                   . 'OR pc.complexion LIKE ? ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
    }

    //echo $sql_where;
    // Ordenaci?n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'pc.id_perfil_criminal ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'pc.apellido_paterno ' . $_GET['typeSort']  . ', pc.apellido_materno ' . $_GET['typeSort']  . ', pc.nombre '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'pc.sexo ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'pc.alias ' . $_GET['typeSort'];
    }  else if ($ind_campo_ord == 5) {
        $sql_order = 'gd.grupo_delictivo ' . $_GET['typeSort'];
    }  else if ($ind_campo_ord == 6) {
        $sql_order = 'sit.situacion ' . $_GET['typeSort'];
    }  else if ($ind_campo_ord == 7) {
        $sql_order = 'nal.nacionalidad ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 8) {
        $sql_order = 'pc.complexion ' . $_GET['typeSort'];
    }
    // Paginaci?n...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objPerfiles->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];


    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["id_perfil_criminal"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_perfil_criminal"] . '">';
                            
                if(!is_null($dato["grupo_delictivo"])){ $GrupoDelictivo=$dato["grupo_delictivo"]; }else{$GrupoDelictivo="<strong>SIN DATO</strong>";}
                if(!is_null($dato["cargo_ocupa"])){ $CargoOcupa=$dato["cargo_ocupa"]; }else{$CargoOcupa="<strong>SIN DATO</strong>";}
                
                if(!is_null($dato["zona_operacion"])){ $ZonaOperacion=$dato["zona_operacion"]; }else{$ZonaOperacion="<strong>SIN DATO</strong>";}                
                if(!is_null($dato["modus_operandis"])){ $ModusOperandis=$dato["modus_operandis"]; }else{$ModusOperandis="<strong>SIN DATO</strong>";}
                //if(!is_null($dato["fotografia_frente"]) and file_exists("ope/mpc/fotografias/". $FotografiaFrente)){$FotografiaFrente=$dato["fotografia_frente"];}else{$FotografiaFrente="sin_fotografia.png";}
            
                //-------------------Tabla para ToolTip --------------------------//
                $TTT='<table id="TablaToolTip">';
                        $TTT.='<tr><td rowspan="10" style="vertical-align: top;"><img class="foto" src="ope/mpc/fotografias/'. $dato["fotografia_frente"] .'" alt="" ></td><td class="clave">ID PERFIL:</td><td class="valor">'.$dato["id_perfil_criminal"].'</td></tr>';                      
                        $TTT.='<tr><td class="clave">SITUACION:</td><td class="valor">'.$dato["situacion"].'</td></tr>';
                        $TTT.='<tr><td class="clave">NOMBRE(S):</td><td class="valor">'.$dato["nombre"]. ' ' . $dato["apellido_paterno"] . ' ' . $dato["apellido_materno"].'</td></tr>';
                        $TTT.='<tr><td class="clave">ALIAS:</td><td class="valor">'.$dato["alias"].'</td></tr>';
                        $TTT.='<tr><td class="clave">SEXO:</td><td class="valor">'.$dato["sexo"].'</td></tr>';
                        $TTT.='<tr><td class="clave">EDAD:</td><td class="valor">'.$dato["edad"].'</td></tr>';
                        //$TTT.='<tr><td class="clave">ESTATURA:</td><td class="valor">'.$dato["estatura"].' m.</td></tr>';
                        $TTT.='<tr><td class="clave">SE�AS PARTICULARES:</td><td class="valor">'.$dato["aspecto"].'</td></tr>';                                               
                        $TTT.='<tr><td class="clave">GRUPO:</td><td class="valor">'. $GrupoDelictivo .'</td></tr>';  
                        $TTT.='<tr><td class="clave">CARGO:</td><td class="valor">'. $CargoOcupa .'</td></tr>';
                        
                        $TTT.='<tr><td class="clave">ZONA DE OPERACION:</td><td class="valor" colspan="2">'. $ZonaOperacion .'</td></tr>';
                        $TTT.='<tr><td class="clave">MODUS OPERANDIS:</td><td class="valor" colspan="2">'. $ModusOperandis .'</td></tr>';
                $TTT.='</table>';
                
                $TTT=str_replace('"',"'",$TTT);       
                                
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 1%;">';
                   // $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 5%; font-size:13px; color:#FF0000;"><strong>' . $dato["id_perfil_criminal"] . '</strong></td>';
       			$nombrePersona = $dato["nombre"]. ' ' . $dato["apellido_paterno"] . ' ' . $dato["apellido_materno"];
                $fotografia="<img src='ope/mpc/fotografias/".$FotografiaFrente."'>";
                $html .= '<td style="text-align: left; width: 15%;"><a href="#" class="Tip" title="'.$TTT.'" rel="ttip">' . $nombrePersona . '</a></td>';
                $html .= '<td style="text-align: center; width: 5%;">' . $dato["sexo"] . '</td>';
                $html .= '<td style="text-align: center; width: 8%;">' . $dato["alias"] . '</td>';
                $html .= '<td style="text-align: left; width: 18%;">' . $GrupoDelictivo . '</td>';
                $html .= '<td style="text-align: center; width: 5%;">' . $dato["situacion"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["nacionalidad"] . '</td>';
                $html .= '<td style="text-align: center; width: 6%;"  title="'.$dato["aspecto"].'" >' . $dato["complexion"] . '</td>';

                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("perfil_panel") . "&id=" . $id_crypt;
                //$url_print = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("ope/mpc/_reportes/rpt_perfil_criminal.php");// . "&id=" . $id_crypt;
                $url_print="ope/mpc/_reportes/rpt_perfil_criminal.php?id=" . $id_crypt;
                $html .= '<td style="text-align: center; width: 7%;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Complementar Ficha Criminal..." ><img src="' . PATH_IMAGES . 'icons/perfil_criminal_24.png" alt="Perfil Criminal ..." /></a>';
                $html .= '  <a href="' . $url_print . '" class="lnkBtnOpcionGrid" title="Imprimir Ficha Criminal ..." target="_blank"><img src="' . PATH_IMAGES . 'icons/doc_pdf24.png" alt="Ficha Criminal" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; font-size:18px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron perfiles criminales con el criterio de b�squeda ...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi?n...";
}
?>