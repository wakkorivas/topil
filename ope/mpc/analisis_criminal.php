<?php
/**
 * @author Markino
 * @copyright 2014
 * MODULO PERFIL CRIMINAL
 */
//utf8_encode();
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
/*include $path . 'includes/class/opetbl_mpc_analisis_criminal.class.php';
$objDataGridAnalisisCriminal = new OpetblMpcAnalisisCriminal();*/
    
//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Analisis Criminal',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                    '<script type="text/javascript" src="includes/js/tinymce_4.0.20/js/tinymce/tinymce.min.js"></script>',
                                    '<link type="text/css" href="ope/mpc/_css/mpc.css" rel="stylesheet"/>',
                                    '<script type="text/javascript" src="ope/mpc/_js/analisis_criminal.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//se reciben parametros
$id_perfil = $objSys->decrypt($_GET["id"]);

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('perfil_panel'). "&id=" . $objSys->encrypt($id_perfil);
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('analisis_criminal_save');
  
  /*
  //$datos = $objDataGridAnalisisCriminal->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
  $datos= $objDataGridAnalisisCriminal->selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='');
    print_r($datos);*/
?>

   <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                   <a href="#" id="btnAgregar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Agregar Nuevos Datos Criminales...">
                      <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                   </a>
                    <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Regresar al Panel del Perfil Criminal...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
     </div>

  
<div id="dvForm-Analisis" class="dvForm-Data">
    <span class="dvForm-Data-pTitle">
        <img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" class="icono"/>
        Analisis Criminal
    </span>
      
<!--****** INICIO TABLA DE ANALISIS CRIMINAL *****-->
    <div id="dvGridAnalisisCriminal" style="border: none; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
          <table class="xGrid-tbSearch">
              <tr>
                  <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
              </tr>
          </table>
          <table class="xGrid-tbCols">
              <tr>
                  <th style="width: 2%; text-align: center;">&nbsp;</th>
                  <th style="width: 5%;" class="xGrid-tbCols-ColSortable">ID</th>
                  <!--<th style="width: 10%;" class="xGrid-thNo-Class">PERFIL_ID</th>-->
                  <th style="width: 15%;" class="xGrid-tbCols-ColSortable">NOMBRE PERFIL</th>
                  <th style="width: 7%;" class="xGrid-tbCols-ColSortable">FECHA</th>
                  <th style="width: 43%;" class="xGrid-thNo-Class">ANALISIS REALIZADO</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ANALISTA</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable"></th>
              </tr>
          </table>
        </div>
        <div class="xGrid-dvBody">
          <table class="xGrid-tbBody">
          
          </table>
        </div>
    </div>
</div>
 <!--****** FIN TABLA DE ABALISIS CRIMINAL ******-->
    
  <!-- Contenido del Formulario -->
  <div id="dvFormAnalisisCriminal" title="SISP :: ">
    <form id="frmAnalisisCriminal" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <fieldset id="fsetopetbl_mpc_analisis_criminal" class="fsetForm-Data">
             <!--</a><legend>opetbl_mpc_analisis_criminal</legend>-->
             <table id="tbfrmopetbl_mpc_analisis_criminal" class="tbForm-Data">
               <tr>
                   <td class="descripcion"><label for="txtIdAnalisisCriminal">Id Analisis Criminal:</label></td>
                   <td class="validation">
                      <input type="text" name="txtIdAnalisisCriminal" id="txtIdAnalisisCriminal" value="0" maxlength="" class="" disabled="disabled" readonly="true" />
                      <span class="pRequerido">*</span>
                    </td>
               </tr>
               <tr>
                   <td class="descripcion"><label for="txtFechaAnalisis">Fecha del Analisis:</label></td>
                   <td class="validation">
                      <input type="text" tabindex="1" name="txtFechaAnalisis" id="txtFechaAnalisis" value="" class="" />
                      <span class="pRequerido">*</span>
                    </td>
               </tr>
               <tr>
                   <td class="descripcion"><label for="txtAreaAnalisisCriminal">Analisis Criminal:</label></td>
                   <td class="validation">
                      <textarea name="txtAreaAnalisisCriminal" id="txtAreaAnalisisCriminal" class="tinymce" rows="10" rows="1" style="width:100%">
                         <!--Editor de Texto para la descripcion de los hechos-->
                      </textarea>
                      <span class="pRequerido">*</span>
                    </td>
               </tr>
             </table>
        </fieldset>
        <!-- Fin del Contenido del Formulario -->
        <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr� continuar hasta que los complete.</p>
        <input type="hidden" name ="hdnUrlDatos" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mpc/_ajax/grid_analisis_criminal.php');?>" />
        <input type="hidden" name ="hdnUrlAdd" id="hdnUrlAdd" value="<?php echo $objSys->encrypt('ope/mpc/_action/analisis_criminal_save.php');?>" />
  </form>
 </div>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
