<?php
/**
 * @author Markino
 * @copyright 2014
 * MODULO PERFIL CRIMINAL
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opecat_mpc_grupos_delictivos.class.php';
$objGpoDelictivo = new OpecatMpcGruposDelictivos();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Datos Criminales',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mpc/_css/mpc.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mpc/_js/datos_criminales.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();


//se reciben parametros
$id_perfil = $objSys->decrypt($_GET["id"]);

$_SESSION['id_perfil']=$objSys->decrypt($_GET["id"]);
//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('perfil_panel'). "&id=" . $objSys->encrypt($id_perfil);
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('datos_criminales_save');

?>
  <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                  <a href="#" id="btnAgregar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Agregar Noevo Dato Criminales ...">
                    <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                  </a>          
                  <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Regresar al Panel del Perfil Criminal...">
                    <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                  </a>
                </td>
            </tr>
        </table>
  </div>
  
  <div id="dvForm-Datos" class="dvForm-Data">
        <span class="dvForm-Data-pTitle">
            <img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" class="icono"/>
             Datos Criminales
        </span>

<!--****** INICIO TABLA DE DATOS  *****-->
    <div id="dvGridDatosCriminales" style="border: none;margin: auto auto; margin-top: 4px; width: auto;">
        <div class="xGrid-dvHeader gradient">
          <table class="xGrid-tbSearch">
              <tr>
                  <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
              </tr>
          </table>
          <table class="xGrid-tbCols">
              <tr>
                  <th style="width: 2%; text-align: center;">&nbsp;</th>
                  <th style="width: 5%;" class="xGrid-tbCols-ColSortable">ID</th>
                  <th style="width: 13%;" class="xGrid-tbCols-ColSortable">NOMBRE</th>
                  <th style="width: 15%;" class="xGrid-tbCols-ColSortable">GRUPO DELICTVO</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable">CARGO</th>
                  <th style="width: 15%;" class="xGrid-tbCols-ColSortable">ZONA DE OPERACION</th>
                  <th style="width: 20%;" class="xGrid-tbCols-ColSortable">BUSCADO POR</th>
                  <th style="width: 10%;" class="xGrid-thNo-Class"></th>
              </tr>
            </table>
        </div>        
        <div class="xGrid-dvBody">
          <table class="xGrid-tbBody">
              <?php

              ?>
          </table>
        </div>
    </div>
</div>
<!--****** FIN TABLA DE DATOS  ******-->

<div id="dvFormDatosCriminales" title="SISP :: ">
<form id="frmDatosCriminales" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
    <fieldset id="fset_datos_criminales" class="fsetForm-Data">
         <!--<legend>opetbl_mpc_datos_criminales</legend>-->    
         <table id="tbfrm_datos_criminales" class="tbForm-Data" style="table-layout: auto;">
           <tr>
               <td class="descripcion"><label for="txtIdDatoCriminal">Dato Criminal Id:</label></td>
               <td class="validation">
                  <input type="text" name="txtIdDatoCriminal" id="txtIdDatoCriminal" readonly="true" placeholder="AUTOMATICO"value="0" maxlength="10" style="width:100px;"/>
                  <!--<span class="pRequerido">*</span>-->
                </td>
           </tr>
           <!--<tr>
               <td class="descripcion"><label for="txtIdPerfilCriminal">Perfil Criminal Id:</label></td>
               <td class="validation">
                  <input type="text" name="txtIdPerfilCriminal" id="txtIdPerfilCriminal" readonly="true" placeholder="AUTOMATICO" value="" maxlength="10" style="width:100px;" />
                 
                </td>
           </tr>-->
           <tr>
               <td class="descripcion"><label for="cbxIdGrupoDelictivo">Grupo Delictivo:</label></td>
               <td class="validation">
                  <select name="cbxIdGrupoDelictivo" id="cbxIdGrupoDelictivo" style="width: 356px;">
                    <?php
                        echo $objGpoDelictivo->getCat_GruposDelictivos();
                    ?>
                    </select>
                  <span class="pRequerido">*</span>
                </td>
           </tr>
           <tr>
               <td class="descripcion"><label for="txtCargoOcupa">Cargo que Ocupa:</label></td>
               <td class="validation">
                  <input type="text" name="txtCargoOcupa" id="txtCargoOcupa" value="" maxlength="100" style="width: 350px;" />
                  <span class="pRequerido">*</span>
                </td>
           </tr>
           <tr>
               <td class="descripcion"><label for="txtZonaOperacion">Zona de Operaci�n:</label></td>
               <td class="validation">
                  <input type="text" name="txtZonaOperacion" id="txtZonaOperacion" value="" maxlength="100" style="width: 350px;" />
                  <span class="pRequerido">*</span>
                </td>
           </tr>
           <tr>
               <td class="descripcion"><label for="txtModusOperandis">Modus Operandis:</label></td>
               <td class="validation">
               <input type="text" name="txtModusOperandis" id="txtModusOperandis" value="" maxlength="100" style="width: 350px;" />
                  <span class="pRequerido">*</span>
                </td>
           </tr>
           <tr>
               <td class="descripcion"><label for="txtBuscadoPor">Incriminado Por:</label></td>
               <td class="validation">
               <input type="text" name="txtBuscadoPor" id="txtBuscadoPor" value="" maxlength="100" style="width: 350px;" />
                  <span class="pRequerido">*</span>
                </td>
           </tr>
           <tr>
               <td class="descripcion"><label for="txtObservaciones">Observaciones:</label></td>
               <td class="validation">
                  <input type="text" name="txtObservaciones" id="txtObservaciones" value="" maxlength="100" style="width: 350px;" />
                </td>
           </tr>
         </table>
    </fieldset>
        <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr� continuar hasta que los complete.</p>
        <input type="hidden" name ="hdnUrlDatos" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mpc/_ajax/grid_datos_criminales.php?id='. $objSys->encrypt($id_perfil));?>" />
        <input type="hidden" name ="hdnUrlSaveDatoCriminal" id="hdnUrlSaveDatoCriminal" value="<?php echo $objSys->encrypt('ope/mpc/_action/datos_criminales_save.php');?>" />
        
 </form>
 </div>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
