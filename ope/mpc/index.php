<?php
/**
 * @author Markinho
 * @copyright 2014
 * Modulo Perfil Criminal
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mpc_perfiles_criminales.class.php';
$objPC=new OpetblMpcPerfilesCriminales();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo'  => 'SISP :: Operativo - Perfil Criminal',
                'usr'     => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mpc/_css/mpc.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mpc/_js/index.js"></script>'
                                   ),
                'header'  => true,
                'menu'    => true,
                'idMenu'  => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>

<?php
//-------------------------------- Barra de opciones ------------------------------------//
?>
<div id="dvTool-Bar" class="dvTool-Bar">
    <table style="width: 100%;">
        <tr>
            <td style="text-align: left; width: 50%;">
                <?php $plantilla->mostrarNombreModulo();?>
            </td>
            <td style="text-align: right; width: 50%;">
                <!-- Botones de opci�n...    -->
                <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' .$objSys->encrypt('perfil_criminal');?>" id="xRegistrar" class="Tool-Bar-Btn" style="width: 90px;" title="Agregar Nuevo Perfil Criminal...">
                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" /><br />Agregar
                </a>
            </td>
        </tr>
    </table>
</div>

<div id="dvGridPerfiles" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
    <div class="xGrid-dvHeader gradient">
        <table class="xGrid-tbSearch">
            <tr>
                <td>Buscar: <input type="text" name="txtBuscar" size="40" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" />
                    <a href="#"  title="Buscar en Perfiles Criminales..." class="xGrid-tbSearch-btnSearch"></a>
                </td>
            </tr>
        </table>
        <table class="xGrid-tbCols">
            <tr>
                <th style="width: 1%;">&nbsp;</th>
                <th style="width: 5%;" class="xGrid-tbCols-ColSortable">ID PERFIL</th>
                <th style="width: 15%;" class="xGrid-tbCols-ColSortable">NOMBRE</th>
                <th style="width: 5%;" class="xGrid-tbCols-ColSortable">SEXO</th>
                <th style="width: 8%;" class="xGrid-tbCols-ColSortable">ALIAS</th>
                <th style="width: 18%;" class="xGrid-tbCols-ColSortable">GRUPO DELICTIVO</th>
                <th style="width: 5%;" class="xGrid-tbCols-ColSortable">SITUACION</th>
                <th style="width: 10%;" class="xGrid-tbCols-ColSortable">NACIONALIDAD</th>
                <th style="width: 6%;" class="xGrid-tbCols-ColSortable">COMPLEXION</th>
                <th style="width: 7%;" class="xGrid-thNo-Class">&nbsp;</th>
            </tr>
          </table>
    </div>
    <div class="xGrid-dvBody">
        <table class="xGrid-tbBody">

        </table>
     </div>
</div>

<input type="hidden" name ="hdnUrlDatos" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mpc/_ajax/index_grid_ajax.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>

