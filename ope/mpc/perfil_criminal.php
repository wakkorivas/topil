<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Perfil Criminal
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opecat_mpc_estados_civiles.class.php';
include 'includes/class/opecat_mpc_nacionalidades.class.php';
include 'includes/class/opecat_mpc_situaciones.class.php';
include 'includes/class/opecat_mpc_grupos_delictivos.class.php';
include 'includes/class/opetbl_mpc_perfiles_criminales.class.php';

$objEdosCiviles     =new OpecatMpcEstadosCiviles();
$objNacionalidades  =new OpecatMpcNacionalidades();
$objSituaciones     =new OpecatMpcSituaciones();
$objGrupoDelictivo  =new OpecatMpcGruposDelictivos();
$objPerfilCriminal = new OpetblMpcPerfilesCriminales();


$IdPerfilCrim=$objSys->decrypt( $_GET["id_perfil"] );
if(!empty($IdPerfilCrim)){
  //Existe un perfil criminal
  //Obtener Infomrmacion del Perfil para llenar el formulario
  $objPerfilCriminal->select($IdPerfilCrim);
  $oper=1; //Edicion
}else{
 //
 $IdPerfilCrim="0"; //Insercion
 $oper=0;  
}
//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Perfil Criminal',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="includes/js/tinymce_4.0.20/js/tinymce/tinymce.min.js"></script>',
                                   '<script type="text/javascript" src="includes/js/file_uploader/AjaxUpload.2.0.min.js"></script>',
                                   '<link type="text/css" href="ope/mpc/_css/mpc.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mpc/_js/perfil_criminal.js"></script>'
                                   ),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
  $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('_action/perfil_criminal_save');
?>
<div id="dvTool-Bar" class="dvTool-Bar">
  <table>
      <tr>
          <td class="tdNombreModulo">
              <?php $plantilla->mostrarNombreModulo(); ?>
          </td>
          <td class="tdBotonesAccion">
              <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Guardar los datos de Alta/Edicion del Perfil Criminal...">
                  <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
              </a>
              <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="width: 90px;" title="Cancelar la Alta/Edicion del Perfil Criminal...">
                  <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
              </a>
          </td>
      </tr>
  </table>
</div>


<!--****************** Formulario de Perfil Criminal *****************************-->
    <form id="frmPerfilCriminal" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Perfil" class="dvForm-Data">
            <span class="dvForm-Data-pTitle">
                <img src="<?php echo PATH_IMAGES;?>icons/perfil_criminal_24.png" class="icono"/>
                Perfil Criminal
            </span>

        <fieldset id="fset_perfil_criminal" class="fsetForm-Data">
            <!-- <legend>Datos Generales</legend> -->

            <table id="tblfrm_perfil_criminal" class="tbForm-Data" style="table-layout: auto;">
             <tr>
                 <td class="descripcion"><label for="txtIdPerfilCriminal">Perfil Criminal No.:</label></td>
                 <td class="validation">
                    <input name="txtIdPerfilCriminal" type="text" class="" id="txtIdPerfilCriminal" style="width:80px;" value="<?php echo $objPerfilCriminal->id_perfil_criminal; ?>" maxlength=""  readonly="true" placeholder="0" />
                    <!-- <span class="pRequerido">*</span> -->
                  </td>
                 <td rowspan="8" class="descripcion">
                 <div id="fotografia" class="fotografia" title="Haga click aqui para agregar una fotografia.">
                    <img src="ope/mpc/fotografias/<?php
                                                    if(!empty($objPerfilCriminal->id_perfil_criminal) && file_exists("ope/mpc/fotografias/".$objPC->fotografia_frente)){
                                                        echo $objPerfilCriminal->fotografia_frente;
                                                    }else{
                                                        echo "sin_fotografia.png";
                                                    }
                                                ?>" class="fotografia" />
                 </div>
                   <!-- <input type="text" name="txtImgName" id="txtImgName" value="" style="width: 200px; display: none;" /> -->
                 <!--<input name="fileFoto" type="file" title=""  value="..." style="width: 160px;"/>--></td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="cbxIdSituacion">Situaci�n:</label></td>
                 <td class="validation">
                    <select name="cbxIdSituacion"  id="cbxIdSituacion" style="width:210px;">
                     <?php
                          echo $objSituaciones->getCat_Situaciones($objPerfilCriminal->id_situacion);
                      ?>
                    </select>
                    <span class="pRequerido">*</span>
                  </td>
              </tr>
             <tr>
                 <td class="descripcion"><label for="txtNombre">Nombre:</label></td>
                 <td class="validation controles">
                   <input type="text" name="txtNombre" id="txtNombre" value="<?php echo $objPerfilCriminal->nombre; ?>" maxlength="35" class="" style="width:250px;" />
                   <span class="pRequerido">*</span>
                 </td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="txtApellidoPaterno">Apellido Paterno:</label></td>
                 <td class="validation controles">
                   <input type="text" name="txtApellidoPaterno" id="txtApellidoPaterno" value="<?php echo $objPerfilCriminal->apellido_paterno; ?>" maxlength="30" class="" style="width:250px;" />
                   <span class="pRequerido">*</span>
                 </td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="txtApellidoMaterno">Apellido Materno:</label></td>
                 <td class="validation controles">
                   <input type="text" name="txtApellidoMaterno" id="txtApellidoMaterno" value="<?php echo $objPerfilCriminal->apellido_materno; ?>" maxlength="30" class="" style="width:250px;" />
                 </td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="txtAlias">Alias:</label></td>
                 <td class="validation">
                   <input type="text" name="txtAlias" id="txtAlias" value="<?php echo $objPerfilCriminal->alias; ?>" maxlength="100" class="" style="width:250px;" />
                   <span class="pRequerido">*</span>
                 </td>
                 </tr>
             <tr>
                  <td class="descripcion"><label>Sexo:</label></td>
                  <td>
                    <div class="dvRadioGroup">
                     <?php 
                        $sexoM=($objPerfilCriminal->sexo=='MASCULINO')? 'checked="true"':'';
                        $sexoF=($objPerfilCriminal->sexo=='FEMENINO')?'checked="true"':'';
                     ?>
                       <label class="label-Radio Masculino" style="margin-right: 10px;"><input type="radio" name="rbnSexo" id="rbnSexo1" value="1" checked="<?php echo $sexoM ?>" />Masculino</label>
                       <label class="label-Radio Femenino"><input type="radio" name="rbnSexo" id="rbnSexo2" value="2" <?php echo $sexoF ?> />Femenino</label>
                    </div>
                 </td>
            </tr>
             <tr>
                 <td class="descripcion"><label for="txtFechaNacimineto">Fecha de Nacimiento:</label></td>
                 <td class="validation">
                    <input type="text" name="txtFechaFechaNacimineto" id="txtFechaNacimineto" style="width:120px" value="<?php echo date('d/m/Y', strtotime($objPerfilCriminal->fecha_nacimineto)); ?>" title="Formato: DD/MM/AAAA" placeholder="DD/MM/AAAA"  maxlength="10"  />
                  </td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="cbxEstadoCivil">Estado Civil:</label></td>
                 <td class="validation">
                    <select name="cbxEstadoCivil" id="cbxEstadoCivil" style="width:210px;">
                      <?php
                          echo $objEdosCiviles->getCat_EstadosCiviles($objPerfilCriminal->id_estado_civil);
                      ?>
                    </select>
                    <span class="pRequerido">*</span>
                 </td>
                 <td class="descripcion"><input type="hidden" name="hdnImgName" id="hdnImgName" value="<?php echo $objPerfilCriminal->fotografia_frente; ?>" /></td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="cbxNacionalidad">Nacionalidad:</label></td>
                 <td class="validation">
                    <select name="cbxNacionalidad" id="cbxNacionalidad" style="width:210px;">
                    <?php
                      echo $objNacionalidades->getCat_Nacionalidades($objPerfilCriminal->id_nacionalidad);
                    ?>
                    </select>
                    <span class="pRequerido">*</span>
                  </td>
                 <td class="descripcion">&nbsp;</td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="txtLugarOrigen">Lugar de Origen:</label></td>
                 <td class="validation">
                    <input type="text" name="txtLugarOrigen" id="txtLugarOrigen" value="<?php echo $objPerfilCriminal->lugar_origen; ?>" maxlength="100" class="" style="width:350px;" />
                  </td>
                 <td class="descripcion">&nbsp;</td>
                 </tr>
              <tr>
                 <td class="descripcion"><label for="txtDomicilio">Domicilio:</label></td>
                 <td class="validation">
                    <input type="text" name="txtDomicilio" id="txtDomicilio" value="<?php echo $objPerfilCriminal->domicilio; ?>" maxlength="100" class="" style="width:350px;" />
                    <!-- <span class="pRequerido">*</span> -->
                  </td>
                 <td class="descripcion">&nbsp;</td>
                 </tr>
             <tr>
                 <td class="descripcion"><label for="txtOcupacion">Ocupaci�n:</label></td>
                 <td class="validation">
                    <input type="text" name="txtOcupacion" id="txtOcupacion" value="<?php echo $objPerfilCriminal->ocupacion; ?>" maxlength="100" class="" style="width:350px;" />
                    <span class="pRequerido">*</span>
                  </td>
                 <td class="descripcion">&nbsp;</td>
             </tr>
             <tr>
              <td colspan="4">&nbsp;</td>
             </tr>

             <tr>
                  <td colspan="3">
                      <fieldset style="border-radius:10px;">
                          <!--<legend>Antrometria B?sica</legend>-->
                          <table style="width: 100%;">
                              <tr>
                                  <td class="derecha"><label for="cbxComplexion">Complexi�n:</label></td>
                                  <td class="validation">
                                    <!-- <input type="text" name="txtComplexion" id="txtComplexion" value="" maxlength="45" class="" style="width:200px;" /> -->
                                  <select name="cbxComplexion" id="cbxComplexion">
                                    <option value="SIN DATO" <?php if($objPerfilCriminal->complexion=="SIN DATO") echo selected; ?>></option>SIN DATO</option>
                                    <option value="DELGADA" <?php if($objPerfilCriminal->complexion=="DELGADA") echo selected; ?>>DELGADA</option>
                                    <option value="REGULAR" <?php if($objPerfilCriminal->complexion=="REGULAR") echo selected; ?>>REGULAR</option>
                                    <option value="ROBUSTA" <?php if($objPerfilCriminal->complexion=="ROBUSTA") echo selected; ?>>ROBUSTA</option>
                                    <option value="ATLETICA" <?php if($objPerfilCriminal->complexion=="ATLETICA") echo selected; ?>>ATLETICA</option>
                                    <option value="OBESA" <?php if($objPerfilCriminal->complexion=="OBESA") echo selected; ?>>OBESA</option>
                                  </select>
                                  </td>
                                  <td></td>
                                  <td class="derecha"><label for="txtCabello">Cabello:</label></td>
                                  <td><input type="text" name="txtCabello" id="txtCabello" value="<?php echo $objPerfilCriminal->cabello; ?>" maxlength="100" class="" style="width:250px;" /></td>
                              </tr>
                              <tr>
                                  <td class="derecha"><label for="txtEstatura">Estatura:</label></td>
                                  <td class="validation">
                                      <input type="text" name="txtEstatura" id="txtEstatura" value="<?php echo $objPerfilCriminal->estatura; ?>" maxlength="4" class="" style="width:60px;" placeholder="m.cm" /> m.
                                  </td>
                                  <td class="validation">
                                  </td>
                                  <td class="derecha"><label for="txtColorPiel">Color de Piel:</label></td>
                                  <td class="validation"><input type="text" name="txtColorPiel" id="txtColorPiel" value="<?php echo $objPerfilCriminal->colorPiel; ?>" maxlength="100" class="" style="width:250px;" /></td>
                              </tr>
                              <tr>
                                  <td class="derecha"><label for="txtPeso">Peso:</label></td>
                                  <td class="validation">
                                      <input type="text" name="txtPeso" id="txtPeso" value="<?php echo $objPerfilCriminal->Peso; ?>" maxlength="6" class="" style="width:60px;" placeholder="k.grs" /> kg.
                                  </td>
                                  <td class="validation">
                                  </td>
                                  <td class="derecha"><label for="txtColorOjos">Color de Ojos:</label></td>
                                  <td><input type="text" name="txtColorOjos" id="txtColorOjos" value="<?php echo $objPerfilCriminal->ColorOjos; ?>" maxlength="100" class="" style="width:250px;" /></td>
                              </tr>
                              <tr>
                                  <td class="derecha"><label for="txtSenasPart">Se�as Particulares:</label></td>
                                  <td class="validation" colspan="4">
                                      <input type="text" name="txtSenasPart" id="txtSenasPart" value="<?php ?>" maxlength="200" class="" style="width:610px;" placeholder="DESCRIBA CICATRICES, TATUAJES, LUNARES, DEFECTOS FISICOS,..." />
                                  </td>
                              </tr>
                          </table>
                      </fieldset>
                  </td>
             </tr>
            </table>
        </fieldset>
<!--**************************************************************************************-->
<input type="hidden" name="oper" id="oper" value="<?php echo $oper; ?>"/>
      <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr� continuar hasta que los complete.</p>
      <input  type="hidden" name="hdnUrlImg" id="hdnUrlImg" value="<?php echo $objSys->encrypt('ope/mpc/uploads/');?>" />
    </form>
</div>
<?php
  //-----------------------------------------------------------------//
  //-- Bloque de cerrado de la plantilla...
  //-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
